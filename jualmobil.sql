/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100421
 Source Host           : localhost:3306
 Source Schema         : jualmobil

 Target Server Type    : MySQL
 Target Server Version : 100421
 File Encoding         : 65001

 Date: 31/08/2022 18:44:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2022_07_10_074524_add_role_table_user', 1);
INSERT INTO `migrations` VALUES (5, '2022_07_15_034631_create_t_mobil_table', 1);
INSERT INTO `migrations` VALUES (6, '2022_07_15_035838_create_t_jual_table', 1);
INSERT INTO `migrations` VALUES (7, '2022_07_15_041233_create_t_wishlist_table', 1);
INSERT INTO `migrations` VALUES (8, '2022_07_15_060338_update_column_table', 1);
INSERT INTO `migrations` VALUES (9, '2022_07_16_164310_create_t_pengecekan_table', 1);
INSERT INTO `migrations` VALUES (10, '2022_07_17_010607_create_t_hasil_pengecekan_table', 1);
INSERT INTO `migrations` VALUES (11, '2022_07_18_035759_add_column_status_t_mobil_table', 1);
INSERT INTO `migrations` VALUES (12, '2022_07_18_213451_add_column_t_mobil_table', 1);
INSERT INTO `migrations` VALUES (13, '2022_07_19_042340_add_column_model_on_t_mobil_table', 1);
INSERT INTO `migrations` VALUES (14, '2022_07_19_061902_add_column_t_pengecekan_table', 1);
INSERT INTO `migrations` VALUES (15, '2022_07_24_131656_add_column_bukti_trf_on_t_pengecekan_table', 1);
INSERT INTO `migrations` VALUES (16, '2022_07_24_131937_add_column_foto4_on_t_mobil_table', 1);
INSERT INTO `migrations` VALUES (17, '2022_07_25_165820_create_t_konstanta_table', 1);
INSERT INTO `migrations` VALUES (18, '2022_08_17_162242_create_t_booking_table', 1);
INSERT INTO `migrations` VALUES (19, '2022_08_25_033804_add_column_on_t_jual_table', 1);
INSERT INTO `migrations` VALUES (20, '2022_08_31_061753_create_t_beli_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for t_beli
-- ----------------------------
DROP TABLE IF EXISTS `t_beli`;
CREATE TABLE `t_beli`  (
  `id_beli` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` int NOT NULL,
  `id_mobil` int NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_beli`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_beli
-- ----------------------------

-- ----------------------------
-- Table structure for t_booking
-- ----------------------------
DROP TABLE IF EXISTS `t_booking`;
CREATE TABLE `t_booking`  (
  `id_booking` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` bigint UNSIGNED NOT NULL,
  `id_mobil` bigint UNSIGNED NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bukti_book` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_booking`) USING BTREE,
  INDEX `t_booking_id_user_foreign`(`id_user`) USING BTREE,
  INDEX `t_booking_id_mobil_foreign`(`id_mobil`) USING BTREE,
  CONSTRAINT `t_booking_id_mobil_foreign` FOREIGN KEY (`id_mobil`) REFERENCES `t_mobil` (`id_mobil`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `t_booking_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_booking
-- ----------------------------

-- ----------------------------
-- Table structure for t_hasil_pengecekan
-- ----------------------------
DROP TABLE IF EXISTS `t_hasil_pengecekan`;
CREATE TABLE `t_hasil_pengecekan`  (
  `id_hasil_pengecekan` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pengecekan` bigint UNSIGNED NOT NULL,
  `keterangan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_hasil_pengecekan`) USING BTREE,
  INDEX `t_hasil_pengecekan_id_pengecekan_foreign`(`id_pengecekan`) USING BTREE,
  CONSTRAINT `t_hasil_pengecekan_id_pengecekan_foreign` FOREIGN KEY (`id_pengecekan`) REFERENCES `t_pengecekan` (`id_pengecekan`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_hasil_pengecekan
-- ----------------------------

-- ----------------------------
-- Table structure for t_jual
-- ----------------------------
DROP TABLE IF EXISTS `t_jual`;
CREATE TABLE `t_jual`  (
  `id_jual` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` bigint UNSIGNED NOT NULL,
  `id_mobil` bigint UNSIGNED NOT NULL,
  `harga_penawaran` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `alamat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `tanggal` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `jam` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id_jual`) USING BTREE,
  INDEX `t_jual_id_user_foreign`(`id_user`) USING BTREE,
  INDEX `t_jual_id_mobil_foreign`(`id_mobil`) USING BTREE,
  CONSTRAINT `t_jual_id_mobil_foreign` FOREIGN KEY (`id_mobil`) REFERENCES `t_mobil` (`id_mobil`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `t_jual_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_jual
-- ----------------------------

-- ----------------------------
-- Table structure for t_konstanta
-- ----------------------------
DROP TABLE IF EXISTS `t_konstanta`;
CREATE TABLE `t_konstanta`  (
  `id_kons` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_kons`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_konstanta
-- ----------------------------
INSERT INTO `t_konstanta` VALUES (1, 'orang', '10');

-- ----------------------------
-- Table structure for t_mobil
-- ----------------------------
DROP TABLE IF EXISTS `t_mobil`;
CREATE TABLE `t_mobil`  (
  `id_mobil` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_mobil` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk_mobil` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bensin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kilometer` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `transmisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kepemilikan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kapasitas_mesin` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pajak` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_mobil` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_mobil` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tahun_produksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_mobil2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `foto_mobil3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `model` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto_mobil4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_mobil`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_mobil
-- ----------------------------

-- ----------------------------
-- Table structure for t_pengecekan
-- ----------------------------
DROP TABLE IF EXISTS `t_pengecekan`;
CREATE TABLE `t_pengecekan`  (
  `id_pengecekan` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` bigint UNSIGNED NOT NULL,
  `tanggal_pengecekan` date NOT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tim` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `bukti_trf` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_pengecekan`) USING BTREE,
  INDEX `t_pengecekan_id_user_foreign`(`id_user`) USING BTREE,
  CONSTRAINT `t_pengecekan_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_pengecekan
-- ----------------------------

-- ----------------------------
-- Table structure for t_wishlist
-- ----------------------------
DROP TABLE IF EXISTS `t_wishlist`;
CREATE TABLE `t_wishlist`  (
  `id_wishlist` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_user` bigint UNSIGNED NOT NULL,
  `id_mobil` bigint UNSIGNED NOT NULL,
  PRIMARY KEY (`id_wishlist`) USING BTREE,
  INDEX `t_wishlist_id_user_foreign`(`id_user`) USING BTREE,
  INDEX `t_wishlist_id_mobil_foreign`(`id_mobil`) USING BTREE,
  CONSTRAINT `t_wishlist_id_mobil_foreign` FOREIGN KEY (`id_mobil`) REFERENCES `t_mobil` (`id_mobil`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `t_wishlist_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_wishlist
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nohp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Admisi', 'admisi@mail.com', NULL, '$2y$10$X.1.8rfoEnUtN20hSY7Seeyc06KiA3mbdDBxee1QuV8Qe.vBFd3qe', '012345', 'jl. admisi', NULL, NULL, NULL, '0');
INSERT INTO `users` VALUES (2, 'Manager', 'manager@mail.com', NULL, '$2y$10$Ph7Tvtz7.2DUlJoTNKiFeuxRsC4AnkpmoDWpXBP04BTfdgyRkxNwu', '0123456', 'jl. manager', NULL, NULL, NULL, '1');
INSERT INTO `users` VALUES (3, 'Marketing Jual', 'mktjual@mail.com', NULL, '$2y$10$RMweecltBtEsEyCkTqCvDOWdtWQFFdgW60UFUmJgXNBSLUua4uE36', '01234567', 'jl. marketing jual', NULL, NULL, NULL, '2');
INSERT INTO `users` VALUES (4, 'Marketing Beli', 'mktbeli@mail.com', NULL, '$2y$10$TLj6vmV0n7SDUw/s697mS.uc6PY73E2pL3cRd3h5VIKmJCR7fkPc2', '012345678', 'jl. marketing beli', NULL, NULL, NULL, '3');
INSERT INTO `users` VALUES (5, 'Jasa Pengecekan', 'jasacek@mail.com', NULL, '$2y$10$07vhFDUofC.qDDIL28ubLOvozNoExifE9sKPxcLBqj15hmA2fi3am', '0123456789', 'jl. pengecekan', NULL, NULL, NULL, '4');
INSERT INTO `users` VALUES (6, 'User', 'user@mail.com', NULL, '$2y$10$jdM9.HY3rVpS326.4Qaxs.AxvILwmrSh/gF6wGFEQp/1UTxOc6E02', '918379', 'jl. user', NULL, NULL, NULL, '5');

SET FOREIGN_KEY_CHECKS = 1;
