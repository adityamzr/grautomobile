<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTMobilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_mobil', function (Blueprint $table) {
            $table->string('tahun_produksi');
            $table->string('tipe', 50);
            $table->string('foto_mobil2')->nullable();
            $table->string('foto_mobil3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_mobil', function (Blueprint $table) {
            $table->dropColumn('tahun_produksi');
            $table->dropColumn('tipe');
            $table->dropColumn('foto_mobil2');
            $table->dropColumn('foto_mobil3');
        });
    }
}
