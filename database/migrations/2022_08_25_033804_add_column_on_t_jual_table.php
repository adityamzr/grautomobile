<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnTJualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_jual', function (Blueprint $table) {
            $table->text('alamat')->nullable();
            $table->text('tanggal')->nullable();
            $table->text('jam')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_jual', function (Blueprint $table) {
            $table->dropColumn('alamat');
            $table->dropColumn('tanggal');
            $table->dropColumn('jam');
        });
    }
}
