<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTMobilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_mobil', function (Blueprint $table) {
            $table->id('id_mobil');
            $table->string('nama_mobil', 100);
            $table->string('merk_mobil', 100);
            $table->string('bensin', 50);
            $table->string('kilometer', 50);
            $table->string('transmisi');
            $table->string('kepemilikan');
            $table->string('kapasitas_mesin', 50);
            $table->string('pajak');
            $table->text('deskripsi');
            $table->string('foto_mobil');
            $table->string('harga_mobil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_mobil');
    }
}
