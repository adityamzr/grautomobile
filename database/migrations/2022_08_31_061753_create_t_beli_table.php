<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTBeliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_beli', function (Blueprint $table) {
            $table->id('id_beli');
            $table->integer('id_user');
            $table->integer('id_mobil');
            $table->string('status');
            $table->string('tanggal');
            $table->string('jam');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_beli');
    }
}
