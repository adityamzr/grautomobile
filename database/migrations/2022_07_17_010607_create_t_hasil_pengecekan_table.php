<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTHasilPengecekanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_hasil_pengecekan', function (Blueprint $table) {
            $table->id('id_hasil_pengecekan');
            $table->unsignedBigInteger('id_pengecekan');
            $table->foreign('id_pengecekan')->references('id_pengecekan')->on('t_pengecekan')->onDelete('cascade');
            $table->string('keterangan', 100);
            $table->text('deskripsi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_hasil_pengecekan');
    }
}
