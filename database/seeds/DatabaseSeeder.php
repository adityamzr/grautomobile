<?php

use App\Konstanta;
use App\Mekanik;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::insert ( [ 
            [
                'nama' => 'Admisi',
                'email' => 'admisi@mail.com',
                'nohp' => '012345',
                'alamat' => 'jl. admisi',
                'password' => bcrypt('12345678'),
                'role' => 0
            ],
            [
                'nama' => 'Manager',
                'email' => 'manager@mail.com',
                'nohp' => '0123456',
                'alamat' => 'jl. manager',
                'password' => bcrypt('12345678'),
                'role' => 1
            ],
            [
                'nama' => 'Marketing Jual',
                'email' => 'mktjual@mail.com',
                'nohp' => '01234567',
                'alamat' => 'jl. marketing jual',
                'password' => bcrypt('12345678'),
                'role' => 2
            ],
            [
                'nama' => 'Marketing Beli',
                'email' => 'mktbeli@mail.com',
                'nohp' => '012345678',
                'alamat' => 'jl. marketing beli',
                'password' => bcrypt('12345678'),
                'role' => 3
            ],
            [
                'nama' => 'Jasa Pengecekan',
                'email' => 'jasacek@mail.com',
                'nohp' => '0123456789',
                'alamat' => 'jl. pengecekan',
                'password' => bcrypt('12345678'),
                'role' => 4
            ],
            [
                'nama' => 'User',
                'email' => 'user@mail.com',
                'nohp' => '918379',
                'alamat' => 'jl. user',
                'password' => bcrypt('12345678'),
                'role' => 5
            ]
        ]);

        Konstanta::insert([
            'nama' => 'orang',
            'value' => '10',
        ]);

        Mekanik::insert([
            [
                'nama_mekanik' => 'Sarul Arifin'
            ],
            [
                'nama_mekanik' => 'Mulyana'
            ],
            [
                'nama_mekanik' => 'Heri Setiawan'
            ],
            [
                'nama_mekanik' => 'Dian Kusdiana'
            ],
            [
                'nama_mekanik' => 'Andri Setiadi'
            ],
            [
                'nama_mekanik' => 'Ade Anwar'
            ],
            [
                'nama_mekanik' => 'Wawan'
            ],
            [
                'nama_mekanik' => 'Agus'
            ],
            [
                'nama_mekanik' => 'Yana Gustiana'
            ],
            [
                'nama_mekanik' => 'Jajang Anwar'
            ],
        ]);
    }
}
