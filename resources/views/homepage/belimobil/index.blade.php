@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Beli</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="/userbelimobil">Beli Mobil</a></li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Daftar Jadwal Kunjungan</h4>
                </div>
                <div class="col-6 text-end">
                    <a href="/produk" class="btn btn-primary"><i data-feather="plus"></i> Tambah Daftar</a>
                </div>
            </div>

            <hr>
            <table class="table table-bordered data-table" id="tabel">
                <thead>
                    <tr>
                        <th class="col-auto">No</th>
                        <th class="col-auto">Tanggal</th>
                        <th class="col-auto">Jam</th>
                        <th class="col-auto">Nama</th>
                        <th class="col-auto">Nomor HP</th>
                        <th class="col-auto">Mobil</th>
                        <th class="col-auto">Tahun</th>
                        <th class="col-auto">Harga</th>
                        {{-- <th class="col-auto">Status</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @php
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');

                            return $hasil_rupiah;
                        }
                    @endphp
                    @foreach ($beli as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->tanggal }}</td>
                        <td>{{ $item->jam }}</td>
                        <td>{{ $item->user->nama }}</td>
                        <td>{{ $item->user->nohp }}</td>
                        <td>{{ $item->mobil->nama_mobil }}</td>
                        <td>{{ $item->mobil->tahun_produksi }}</td>
                        <td>Rp{{ titik($item->mobil->harga_mobil) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection