@extends('layouts.home')

@section('content')
    <div class="breadcrumb-wrap mt-4">
        <div class="container-fluid">
            <h1 class="h2 mb-3"><strong>List</strong> Produk</h1>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Products</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->
    
    <!-- Product List Start -->
    <div class="product-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row outer">
                        @php
                            function titik($angka){
                                $hasil_rupiah = "" . number_format($angka,0,'','.');

                                return $hasil_rupiah;
                            }
                        @endphp
                        @foreach ($produk as $item)
                        <div class="col-md-4" id="product-body">
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="#">{{ $item->nama_mobil }}</a>
                                    <div>
                                    <span class="" style="color: white">Tipe {{ $item->tipe }} - Tahun {{ $item->tahun_produksi }}</span>
                                    </div>
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.html">
                                        <img src="{{ asset("storage/back/foto_mobil/$item->foto_mobil") }}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a class="btn-like" data-id-mobil="{{ $item->id_mobil }}"><i class="fa fa-heart"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>Rp</span>{{ titik($item->harga_mobil) }}</h3>
                                    <a class="btn" href={{ url("produk/".$item->id_mobil) }}><i class="fa fa-eye"></i>Detail</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                    
                    <!-- Pagination Start -->
                    <div class="col-md-12 text-center mb-4">
                        <nav aria-label="Page navigation example">
                            <a href="#" class="btn btn-primary" id="showMore">Show More</a>
                        </nav>
                    </div>
                    <!-- Pagination Start -->
                </div>          
            
                <!-- Side Bar End -->
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>
var itemsCount = 0,
    itemsMax = $('.outer #product-body').length;
$('.outer #product-body').hide();

function showNextItems() {
    var pagination = 6;
    
    for (var i = itemsCount; i < (itemsCount + pagination); i++) {
        $('.outer #product-body:eq(' + i + ')').show();
        // console.log(i);
    }

    itemsCount += pagination;
    // console.log(pagination);
    
    if (itemsCount > itemsMax) {
        $('#showMore').hide();
    }
}

showNextItems();

$('#showMore').on('click', function (e) {
    e.preventDefault();
    showNextItems();
});

$('.btn-like').on('click', function(){
    const id = $(this).data('id-mobil')
    // console.log(id)
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "/produk/like",
        type: 'POST',
        data: {
        "id": id,
        "_token": "{{ csrf_token() }}"
        },
        success: function(data){
            Swal.fire({
            position: 'top-center',
            icon: 'success',
            title: 'Success!',
            text: data['message'],
            showConfirmButton: false,
            timer: 1500
            })
        }
    });
});
</script>
@endpush