@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Booking</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="/userbooking">Booking Mobil</a></li>
                {{-- <li class="breadcrumb-item active" aria-current="page">Data</li> --}}
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Daftar Booking</h4>
                </div>
                {{-- <div class="col-6 text-end">
                    <a href="/userpengecekan/create" class="btn btn-primary"><i data-feather="plus"></i> Tambah Booking</a>
                </div> --}}
            </div>

            <hr>
            <table class="table table-bordered data-table" id="tabel">
                <thead>
                    <tr>
                        <th class="col-auto">No</th>
                        <th class="col-auto">Foto</th>
                        <th class="col-auto">Mobil</th>
                        <th class="col-auto">Merk</th>
                        <th class="col-auto">Tahun</th>
                        <th class="col-auto">Harga Mobil</th>
                        <th class="col-auto">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($booking as $item)
                    @php
                        $foto = $item->mobil->foto_mobil;
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');

                            return $hasil_rupiah;
                        }
                    @endphp
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td><img src="{{ asset("storage/back/foto_mobil/$foto") }}" alt="Foto Mobil" width="50%" height="60px"></td>
                        <td>{{ $item->mobil->nama_mobil }}</td>
                        <td>{{ $item->mobil->merk_mobil }}</td>
                        <td>{{ $item->mobil->tahun_produksi }}</td>
                        <td>{{ titik($item->mobil->harga_mobil) }}</td>
                        <td>
                            @if ($item->status == 'Booking')
                            {{ $item->status }}
                            @elseif ($item->status == 'Tolak')
                            Ditolak
                            @else
                            {{ $item->status }}
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });
</script>
@endpush