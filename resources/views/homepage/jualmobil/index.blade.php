@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Jual</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="/jualmobil">Jual Mobil</a></li>
                {{-- <li class="breadcrumb-item active" aria-current="page">Data</li> --}}
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Daftar Pengajuan Jual</h4>
                </div>
                <div class="col-6 text-end">
                    <a href="/jualmobil/create" class="btn btn-primary"><i data-feather="plus"></i> Jual Mobil</a>
                </div>
            </div>

            <hr>
            <table class="table table-bordered data-table" id="tabel">
                <thead>
                    <tr>
                        <th class="col-auto">No</th>
                        <th class="col-auto">Foto</th>
                        <th class="col-auto">Nama</th>
                        <th class="col-auto">Merk</th>
                        <th class="col-auto">Transmisi</th>
                        <th class="col-auto">Harga</th>
                        <th class="col-auto">Status</th>
                        <th class="col-auto text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');

                            return $hasil_rupiah;
                        }
                    @endphp
                    @foreach ($jual as $item)
                    @php
                        $foto = $item->mobil->foto_mobil;
                    @endphp
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="text-center"><img src="{{ asset("storage/back/foto_mobil/$foto") }}" width="50%" height="60px" alt="Foto Mobil"></td>
                        <td>{{ $item->mobil->nama_mobil }}</td>
                        <td>{{ $item->mobil->merk_mobil }}</td>
                        <td>{{ $item->mobil->transmisi }}</td>
                        <td>Rp{{ titik($item->harga_penawaran) }}</td>
                        <td>
                            {{ $item->mobil->status }}
                        </td>
                        <td class="text-center">
                            <a href="{{ url('jualmobil/'.$item->id_jual) }}" class="btn btn-info"><i data-feather="eye"></i> Detail</a>
                            @if ($item->mobil->status == 'Pengajuan')
                                <a href="{{ url('jualmobil/'.$item->id_jual.'/edit') }}" class="btn btn-warning"><i data-feather="edit"></i> Edit</a>
                                <button type="button" class="btn btn-danger btn-hapus" data-id-jual="{{ $item->id_jual }}" data-id-nama="{{ $item->mobil->nama_mobil }}"><i data-feather="trash"></i> Hapus</button>
                            @elseif($item->mobil->status == 'Nego' && $item->alamat == null)
                                <button type="button" data-id="{{ $item->id_jual }}" class="btn btn-primary btn-nego">Isi Jadwal</button>
                            @elseif($item->mobil->status == 'Tolak')

                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="negoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jadwal Negosiasi</h5>
                <button type="button" class="btn-close bc" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="/jualmobil/nego" method="POST" class="needs-validation" novalidate>
                @csrf
            <div class="modal-body">
                    <input type="hidden" id="jual_id" name="id_jual">
                    <div class="col-12 mb-2">
                        <label for="" class="form-label">Alamat</label>
                        <textarea name="alamat" id="" class="form-control" cols="10" rows="5" required></textarea>
                        <div class="invalid-feedback">
                            Masukan Alamat.
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <label for="" class="form-label">Tanggal</label>
                        <input type="date" class="form-control" name="tanggal" required>
                        <div class="invalid-feedback">
                            Masukan Tanggal.
                        </div>
                    </div>
                    <div class="col-12 mb-2">
                        <label for="" class="form-label">Jam</label>
                        <input type="time" class="form-control" name="jam" required>
                        <div class="invalid-feedback">
                            Masukan Jam.
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary bc" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
            }

            form.classList.add('was-validated')
        }, false)
        })
    })()
        
    $(document).ready(function () {
        $('#tabel').DataTable();
    });

    $('.btn-hapus').on('click', function(){
        const id = $(this).data('id-jual')
        const nama = $(this).data('id-nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Pengajuan Penjualan Mobil '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "jualmobil/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })

    $('body').on('click', '.btn-nego', function(){
        $('#negoModal').modal('show')
        const id = $(this).data('id')
        console.log(id)
        $('#jual_id').val(id)
    })
    $('body').on('click', '.bc', function(){
        $('#negoModal').modal('hide')
    })
</script>
@endpush