@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Jual</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/jualmobil">Jual Mobil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Detail</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Detail Pengajuan Jual</h4>
                </div>
            </div>
            <hr>
            <div class="card">
                <div class="card-body">
                    <div class="col-6 mb-4">
                        @php
                            $foto = $jual->mobil->foto_mobil;
                            $foto2 = $jual->mobil->foto_mobil2;
                            $foto3 = $jual->mobil->foto_mobil3;
                            $foto4 = $jual->mobil->foto_mobil4;
                            function titik($angka){
                                $hasil_rupiah = "" . number_format($angka,0,'','.');

                                return $hasil_rupiah;
                            }
                        @endphp
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$foto") }}" height="300px" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$foto2") }}" height="300px" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$foto3") }}" height="300px" alt="Third slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$foto4") }}" height="300px" alt="Third slide">
                                    </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->nama_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-4">Merk Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->merk_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-4">Model Unit</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->model }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-4">Tipe</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->tipe }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-4">Tahun Produksi</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->tahun_produksi }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-4">Bensin</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->bensin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Kilometer</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->kilometer }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Transmisi</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->transmisi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Kepemilikan</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->kepemilikan }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Kapasitas Mesin</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->kapasitas_mesin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Pajak</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->pajak }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Deskripsi</div>
                        <div class="col-1">:</div>
                        <div class="col-7">{{ $jual->mobil->deskripsi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        @if ($jual->mobil->status == 'Deal')
                        <div class="col-4">Harga</div>    
                        @else
                        <div class="col-4">Harga Penawaran</div>
                        @endif
                        <div class="col-1">:</div>
                        <div class="col-7">Rp{{ titik($jual->harga_penawaran) }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-4">Status</div>
                        <div class="col-1">:</div>
                        <div class="col-7">
                            @if ($jual->mobil->status == 'Pengajuan')
                                <span class="badge bg-primary">{{ $jual->mobil->status }}</span>
                            @elseif($jual->mobil->status == 'Nego')
                                <span class="badge bg-warning">{{ $jual->mobil->status }}</span>
                            @else
                                <span class="badge bg-success">Deal</span>
                            @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        var  nego = $('#ubah_status').find('option:selected').val();
        if(nego  === 'Nego'){
                $('#harga_nego').prop('readonly', false)
            }else{
                $('#harga_nego').prop('readonly',true)
            }

        $('#ubah_status').on('change', function(){
            const status = $(this).val()
            if(status  === 'Nego'){
                $('#harga_nego').prop('readonly', false)
            }else{
                $('#harga_nego').prop('readonly',true)
            }
        })
</script>
@endpush