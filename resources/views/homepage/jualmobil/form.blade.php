@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Jual</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/jualmobil">Jual Mobil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Form Pengajuan Jual</h4>
                </div>
            </div>
            <hr>
            <form action="{{ url('/jualmobil', @$jual->id_jual) }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                @csrf
                @if (!empty($jual))
                    @method('PATCH')
                @endif
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <label for="" class="form-label">Nama Mobil</label>
                            <input type="text" class="form-control" name="nama_mobil" id="" value="{{ old('nama_mobil', @$jual->mobil->nama_mobil) }}" required>
                            <div class="invalid-feedback">
                                Isi Nama Mobil.
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="" class="form-label">Merk Mobil</label>
                            <select class="form-select" name="merk_mobil" id="" required>
                                <option value="">--Pilih Merk--</option>
                                <option value="Honda" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Honda' ? 'selected' : '' }}>Honda</option>
                                <option value="Toyota" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Toyota' ? 'selected' : '' }}>Toyota</option>
                                <option value="Suzuki" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Suzuki' ? 'selected' : '' }}>Suzuki</option>
                                <option value="Mitsubishi" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Mitsubishi' ? 'selected' : '' }}>Mitsubishi</option>
                                <option value="BMW" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'BMW' ? 'selected' : '' }}>BMW</option>
                                <option value="Mercedez" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Mercedez' ? 'selected' : '' }}>Mercedez</option>
                                <option value="Daihatsu" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Daihatsu' ? 'selected' : '' }}>Daihastsu</option>
                                <option value="Wuling" {{ old('merk_mobil', @$jual->mobil->merk_mobil) == 'Wuling' ? 'selected' : '' }}>Wuling</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Merk Mobil.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <label for="" class="form-label">Bahan Bakar</label>
                            <select class="form-select" name="bensin" id="" required>
                                <option value="">--Pilih Bahan Bakar--</option>
                                <option value="Bensin" {{ old('bensin', @$jual->mobil->bensin) == 'Bensin' ? 'selected' : '' }}>Bensin</option>
                                <option value="Diesel" {{ old('bensin', @$jual->mobil->bensin) == 'Diesel' ? 'selected' : '' }}>Diesel</option>
                                <option value="BBG" {{ old('bensin', @$jual->mobil->bensin) == 'BBG' ? 'selected' : '' }}>BBG (Gas)</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Bahan Bakar.
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="" class="form-label">Kilometer</label>
                            <input type="number" class="form-control" name="kilometer" value="{{ old('kilometer', @$jual->mobil->kilometer) }}" id="" required>
                            <div class="invalid-feedback">
                                Isi Kilometer.
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="" class="form-label">Transmisi</label>
                            <select class="form-select" name="transmisi" id="" required>
                                <option value="">--Pilih Transmisi--</option>
                                <option value="Manual" {{ old('transmisi', @$jual->mobil->transmisi) == 'Manual' ? 'selected' : '' }}>Manual</option>
                                <option value="Otomatis" {{ old('transmisi', @$jual->mobil->transmisi) == 'Otomatis' ? 'selected' : '' }}>Otomatis</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Transmisi.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-4">
                            <label for="" class="form-label">Kapasitas Mesin</label>
                            <input type="number" class="form-control" name="kapasitas_mesin" value="{{ old('kapasitas_mesin', @$jual->mobil->kapasitas_mesin) }}" id="" required>
                            <div class="invalid-feedback">
                                Isi Kapasitas Mesin.
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="" class="form-label">Kepemilikan</label>
                            <select class="form-select" name="kepemilikan" id="" required>
                                <option value="">--Pilih Kepemilikan--</option>
                                <option value="Pertama" {{ old('kepemilikan', @$jual->mobil->kepemilikan) == 'Pertama' ? 'selected' : '' }}>Pertama</option>
                                <option value="Kedua" {{ old('kepemilikan', @$jual->mobil->kepemilikan) == 'Kedua' ? 'selected' : '' }}>Kedua</option>
                                <option value="Ketiga" {{ old('kepemilikan', @$jual->mobil->kepemilikan) == 'Ketiga' ? 'selected' : '' }}>Ketiga</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Kepemilikan.
                            </div>
                        </div>
                        <div class="col-4">
                            <label for="" class="form-label">Pajak</label>
                            <input type="date" class="form-control" name="pajak" value="{{ old('pajak', @$jual->mobil->pajak) }}" id="datepicker" required>
                            <div class="invalid-feedback">
                                Pilih Tanggal Pajak.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-6">
                            <label for="" class="form-label">Model Unit</label>
                            <select class="form-select" name="model" id="" required>
                                <option value="">--Pilih Model--</option>
                                <option value="SUV" {{ old('model', @$jual->mobil->model) == 'SUV' ? 'selected' : '' }}>SUV</option>
                                <option value="Sedan" {{ old('model', @$jual->mobil->model) == 'Sedan' ? 'selected' : '' }}>Sedan</option>
                                <option value="Double" {{ old('model', @$jual->mobil->model) == 'Double' ? 'selected' : '' }}>Double-Cabin</option>
                                <option value="Pickup" {{ old('model', @$jual->mobil->model) == 'Pickup' ? 'selected' : '' }}>Pickup</option>
                                <option value="Diesel" {{ old('model', @$jual->mobil->model) == 'Diesel' ? 'selected' : '' }}>Diesel</option>
                                <option value="City" {{ old('model', @$jual->mobil->model) == 'City' ? 'selected' : '' }}>City Car</option>
                                <option value="Sport" {{ old('model', @$jual->mobil->model) == 'Sport' ? 'selected' : '' }}>Sport</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Model Unit.
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="" class="form-label">Tahun Produksi</label>
                            <input type="number" class="form-control" name="tahun_produksi" value="{{ old('tahun_produksi', @$jual->mobil->tahun_produksi) }}" id="" required>
                            <div class="invalid-feedback">
                                Pilih Tahun Produksi.
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <label for="" class="form-label">Tipe</label>
                        <input type="text" class="form-control" placeholder="Contoh: Sport Dakar 2X4" name="tipe" id="" value="{{ old('tipe', @$jual->mobil->tipe) }}" required>
                        <div class="invalid-feedback">
                            Isi Tipe.
                        </div>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Deskrpisi</label>
                        <div class="mb-2">
                            <span style="color: red">*</span><span>Note: Diisi dengan detail yang mobil yang lebih merinci</span>
                        </div>
                        <textarea class="mb-0 form-control" rows="3" cols="10" name="deskripsi">{{ old('deskripsi', @$jual->mobil->deskripsi) }}</textarea>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Foto Mobil 1</label>
                        <input type="file" class="form-control" name="foto_mobil" id="">
                        *Disarankan angle foto 360 derajat/foto interior
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Foto Mobil 2</label>
                        <input type="file" class="form-control" name="foto_mobil2" id="">
                        *Disarankan angle foto 360 derajat/foto interior
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Foto Mobil 3</label>
                        <input type="file" class="form-control" name="foto_mobil3" id="">
                        *Disarankan angle foto 360 derajat/foto interior
                    </div>
                    <div class="col-12 mb-3">
                        <label for="" class="form-label">Foto Mobil 4</label>
                        <input type="file" class="form-control" name="foto_mobil4" id="">
                        *Disarankan angle foto 360 derajat/foto interior
                    </div>
                    <div class="col-12 mb-4">
                        <label for="" class="form-label">Harga Mobil</label>
                        <input type="number" class="form-control" name="harga_mobil" id="inputHarga" value="{{ old('harga_mobil', @$jual->mobil->harga_mobil) }}" required>
                        <div class="invalid-feedback">
                            Isi Harga Mobil.
                        </div>
                        <p class="mt-2">Rp<span id="hargaMobil"></span></p>
                    </div>
                    {{-- Status --}}
                    <input type="hidden" class="form-control" name="status" id="" value="{{ old('status', @$jual->mobil->status) }}">
                    <div class="col-12 mb-4 text-end">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        $('#inputHarga').keyup(function() {
            harga = $('#inputHarga').val();
            temp = harga;
            x = +harga;
            console.log(x);
            
            var number_string = temp.toString(),
                sisa = number_string.length % 3,
                rupiah = number_string.substr(0, sisa),
                ribuan = number_string.substr(sisa).match(/\d{3}/g);

                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                    var rp = 'Rp' + rupiah;
                }
            $('#hargaMobil').text(rupiah);
        })
    </script>
@endpush