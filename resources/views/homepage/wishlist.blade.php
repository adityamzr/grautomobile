@extends('layouts.home')

@section('content')
    <div class="breadcrumb-wrap mt-4">
        <div class="container-fluid">
            <h1 class="h2 mb-3"><strong>Wishlist</h1>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Wishlist</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->
    
    <!-- Product List Start -->
    <div class="product-view">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row outer">
                        @php
                            function titik($angka){
                                $hasil_rupiah = "" . number_format($angka,0,'','.');

                                return $hasil_rupiah;
                            }
                        @endphp
                        @foreach ($produk as $item)
                        <div class="col-md-4" id="product-body">
                            <div class="product-item">
                                <div class="product-title">
                                    <a href="#">{{ $item->mobil->nama_mobil }}</a>
                                    <div>
                                    <span class="" style="color: white">Tipe {{ $item->mobil->tipe }} - Tahun {{ $item->mobil->tahun_produksi }}</span>
                                    </div>
                                </div>
                                @php
                                    $foto = $item->mobil->foto_mobil;
                                @endphp
                                <div class="product-image">
                                    <a href="product-detail.html">
                                        <img src="{{ asset("storage/back/foto_mobil/$foto") }}" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                        <a class="btn-hapus" data-id="{{ $item->id_wishlist }}" data-nama="{{ $item->mobil->nama_mobil }}"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3><span>Rp</span>{{ titik($item->mobil->harga_mobil) }}</h3>
                                    <a class="btn" href={{ url("produk/".$item->mobil->id_mobil) }}><i class="fa fa-eye"></i>Detail</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        
                    </div>
                    
                    <!-- Pagination Start -->
                    <div class="col-md-12 text-center mb-4">
                        <nav aria-label="Page navigation example">
                            <a href="#" class="btn btn-primary" id="showMore">Show More</a>
                        </nav>
                    </div>
                    <!-- Pagination Start -->
                </div>          
            
                <!-- Side Bar End -->
            </div>
        </div>
    </div>
@endsection

@push('script')
<script>
var itemsCount = 0,
    itemsMax = $('.outer #product-body').length;
$('.outer #product-body').hide();

function showNextItems() {
    var pagination = 6;
    
    for (var i = itemsCount; i < (itemsCount + pagination); i++) {
        $('.outer #product-body:eq(' + i + ')').show();
        // console.log(i);
    }

    itemsCount += pagination;
    // console.log(pagination);
    
    if (itemsCount > itemsMax) {
        $('#showMore').hide();
    }
}

showNextItems();

$('#showMore').on('click', function (e) {
    e.preventDefault();
    showNextItems();
});

$('.btn-hapus').on('click', function(){
    const id = $(this).data('id')
    const nama = $(this).data('nama')
    // console.log(id)
    Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Wishlist Mobil '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "wishlist/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
});
</script>
@endpush