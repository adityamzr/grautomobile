@extends('layouts.home')

@section('content')
    <div class="breadcrumb-wrap mt-4">
        <div class="container-fluid">
            <h1 class="h2 mb-3"><strong>Detail</strong> Produk</h1>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/produk">Products</a></li>
                <li class="breadcrumb-item active">Detail Product</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumb End -->
    @php
        function titik($angka){
            $hasil_rupiah = "" . number_format($angka,0,'','.');

            return $hasil_rupiah;
        }
    @endphp
    <!-- Product List Start -->
    <div class="container my-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-6 offset-1">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil") }}" height="400px" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil2") }}" height="400px" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil3") }}" height="400px" alt="Third slide">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="product-content mx-3">
                            <div class="title"><h2>{{ $mobil->nama_mobil }}</h2></div>
                            <div class="my-2 harga">
                                <span>Price:</span>
                                <h4>Rp{{ titik($mobil->harga_mobil) }}</h4>
                            </div>
                            <div class="my-2 merk">
                                <span>Merk:</span>
                                <h4>{{ $mobil->merk_mobil }}</h4>
                            </div>
                            <div class="my-2">
                                <span>Tahun:</span>
                                <h4>{{ $mobil->tahun_produksi }}</h4>
                            </div>
                            <div class="my-2 pajak">
                                <span>Pajak:</span>
                                <h4>{{ $mobil->pajak }}</h4>
                            </div>
                            <div class="row">
                                <div class="col-6 mt-4">
                                    <a class="btn w-100" href="/produk"><i class="fa fa-shopping-cart"></i> Lihat Produk Lainnya</a>
                                </div>
                                @if (Auth::User())
                                <div class="col-6 mt-4">
                                    @if ($beli)
                                        <button type="button" class="btn btn-secondary w-100"  disabled><i class="fas fa-bookmark"></i> Sudah Dibooking</button>
                                    @else
                                        <button type="button" data-id="{{ $mobil->id_mobil }}" data-id-user="{{ Auth::user()->id }}" class="btn btn-secondary w-100 btn-nego"><i class="fas fa-bookmark"></i> Booking Sekarang</button>
                                    @endif
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="row product-detail-bottom my-4">
                    <div class="col-lg-12 my-3">
                        <ul class="nav nav-pills nav-justified">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#description" style="color: white">Deskripsi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#specification" style="color: white">Spesifikasi</a>
                            </li>
                        </ul>

                        <div class="tab-content my-3">
                            <div id="description" class="container tab-pane active">
                                <h4>Deskripsi Produk</h4>
                                <p>
                                {{ $mobil->deskripsi }}
                                </p>
                            </div>
                            <div id="specification" class="container tab-pane fade">
                                <h4>Spesifikasi Produk</h4>
                                <ul>
                                    <li>Model: <span>{{ $mobil->model }}</span></li>
                                    <li>Tipe: <span>{{ $mobil->tipe }}</span></li>
                                    <li>Kilometer: <span>{{ $mobil->kilometer }}</span> km/h</li>
                                    <li>Bensin: <span>{{ $mobil->bensin }}</span></li>
                                    <li>Transmisi: <span>{{ $mobil->transmisi }}</span></li>
                                    <li>Kapasitas Mesin: <span>{{ $mobil->kapasitas_mesin }}</span></li>
                                    <li>Kepemilikan: <span>{{ $mobil->kepemilikan }}</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="negoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Jadwal Kunjungan/Negosiasi</h5>
                    <button type="button" class="btn-close bc" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="/belimobil" method="POST" class="needs-validation" novalidate>
                    @csrf
                <div class="modal-body">
                        <input type="hidden" id="mobil_id" name="id_mobil">
                        <input type="hidden" id="user_id" name="id_user">
                        <div class="col-12 mb-2">
                            <label for="" class="form-label">Tanggal</label>
                            <input type="date" class="form-control" name="tanggal" required>
                            <div class="invalid-feedback">
                                Masukan Tanggal.
                            </div>
                        </div>
                        <div class="col-12 mb-2">
                            <label for="" class="form-label">Jam</label>
                            <input type="time" class="form-control" name="jam" required>
                            <div class="invalid-feedback">
                                Masukan Jam.
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary bc" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
            }

            form.classList.add('was-validated')
        }, false)
        })
    })()

    $('body').on('click', '.btn-nego', function(){
        $('#negoModal').modal('show')
        const id = $(this).data('id')
        const id_user = $(this).data('id-user')
        console.log(id)
        $('#mobil_id').val(id)
        $('#user_id').val(id_user)
    })
    $('body').on('click', '.bc', function(){
        $('#negoModal').modal('hide')
    })
    </script>
@endpush