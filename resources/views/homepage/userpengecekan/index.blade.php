@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Pengecekan</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="/userpengecekan">Pengecekan Mobil</a></li>
                {{-- <li class="breadcrumb-item active" aria-current="page">Data</li> --}}
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-6">
                    <h4>Daftar Pengecekan</h4>
                </div>
                <div class="col-6 text-end">
                    <a href="/userpengecekan/create" class="btn btn-primary"><i data-feather="plus"></i> Tambah Jadwal</a>
                </div>
            </div>

            <hr>
            <table class="table table-bordered data-table" id="tabel">
                <thead>
                    <tr>
                        <th class="col-auto">No</th>
                        <th class="col-auto">Nama</th>
                        <th class="col-auto">Nomor Handphone</th>
                        <th class="col-auto">Alamat</th>
                        <th class="col-auto">Tanggal Pengecekan</th>
                        <th class="col-auto">Jam</th>
                        <th class="col-auto">Mekanik</th>
                        <th class="col-auto">Status</th>
                        <th class="col-auto text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pengecekan as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->user->nama }}</td>
                        <td>{{ $item->user->nohp }}</td>
                        <td>{{ $item->user->alamat }}</td>
                        <td>{{ $item->tanggal_pengecekan }}</td>
                        <td>{{ $item->jam }}</td>
                        <td class="text-center">
                            @if ($item->status == 'Terima')
                                {{ $item->mekanik->nama_mekanik }}
                            @else
                                <span>-</span>
                            @endif    
                        </td>
                        <td>
                            @if ($item->status == 'Terima')
                            Diproses
                            @elseif ($item->status == 'Menunggu')
                            Menunggu Pembayaran
                            @elseif ($item->status == 'Tolak')
                            Ditolak
                            @else
                            {{ $item->status }}
                            @endif
                        </td>
                        <td class="text-center">
                            @if ($item->status == 'Selesai')
                                <a href="{{ url('userpengecekan/'.$item->id_pengecekan) }}" class="btn btn-info"><i data-feather="eye"></i> Lihat Hasil</a>
                            @elseif($item->status == 'Pengajuan')
                                <a href="{{ url('userpengecekan/'.$item->id_pengecekan.'/edit') }}" class="btn btn-warning"><i data-feather="edit"></i> Edit</a>
                                <button type="button" class="btn btn-danger btn-hapus" data-id-pengecekan="{{ $item->id_pengecekan }}" data-id-tanggal="{{ $item->tanggal_pengecekan }}"><i data-feather="trash"></i> Hapus</button>
                            @elseif($item->status == 'Tolak')
                                <button type="button" class="btn btn-danger btn-hapus" data-id-pengecekan="{{ $item->id_pengecekan }}" data-id-tanggal="{{ $item->tanggal_pengecekan }}"><i data-feather="trash"></i> Hapus</button>
                            @elseif($item->status == 'Menunggu')
                                <button type="button" class="btn btn-primary btn-modal" data-id-pengecekan="{{ $item->id_pengecekan }}"><i data-feather="upload"></i> Upload Bukti</button>
                            @else
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="uploadBukti" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran</h5>
          <button type="button" class="btn-close" id="btn-cross" aria-label="Close"></button>
        </div>
        <form action="{{ url('/userpengecekan/upload') }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
            @csrf
            <div class="modal-body">
                <p class="mb-0"><span class="text-danger">*</span>Note:</p>
                <p class="mb-0">Harga 1 kali pengecekan Rp300.000, termasuk scan ECU/CPU pada mobil dan pengecekan 175 titik.</p>
                <p> Nomor rekening: <strong>1320025551533</strong> (Mandiri)</p>
                <input type="hidden" name="id_pengecekan" id="id_pengecekan" value="">
                <input type="file" class="form-control" name="bukti_trf" required>
                <div class="invalid-feedback">
                    Wajib upload bukti pembayaran.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="btn-close">Close</button>
                <button type="submit" class="btn btn-primary">Upload</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@push('script')
<script type="text/javascript">
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
            }

            form.classList.add('was-validated')
        }, false)
        })
    })()

    $(document).ready(function () {
        $('#tabel').DataTable();
    });

    $('body').on('click', '.btn-modal', function(){
        $('#uploadBukti').modal('show') 
        const id = $(this).data('id-pengecekan')
        $('#id_pengecekan').val(id)
    })

    $('#btn-close').on('click', function(){
            $('#uploadBukti').modal('hide')
        })

    $('#btn-cross').on('click', function(){
        $('#uploadBukti').modal('hide')
    })

    $('.btn-hapus').on('click', function(){
        const id = $(this).data('id-pengecekan')
        const tgl = $(this).data('id-tanggal')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Pengecekan Tanggal '+tgl+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "userpengecekan/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
</script>
@endpush