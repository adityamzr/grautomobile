@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Pengecekan</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item"><a href="/userpengecekan">Pengecekan Mobil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Form</li>
            </ol>
        </nav>
    </div>
    <div class="card col-6 p-3">
        <div class="card-body">
            <div class="row my-3">
                <div class="col-12">
                    <h4 class="mb-3">Form Pengajuan Jadwal</h4>
                    <p><span class="text-danger">*</span>Harga 1 kali pengecekan Rp300.000, termasuk scan ECU/CPU pada mobil dan pengecekan 175 titik. Nomor rekening: <strong>1320025551533</strong> (Mandiri)</p>
                    <p class="mb-0"><span class="text-danger">*</span>Tim Pengecekan yang tersedia: <strong>{{ $mekanik }} orang</strong>.</p>
                </div>
            </div>
            <hr>
            <form action="{{ url('/userpengecekan', @$pengecekan->id_pengecekan) }}" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
                @csrf
                @if (!empty($pengecekan))
                    @method('PATCH')
                @endif
                <input type="hidden" name="kons" value="{{ $mekanik }}">
                <div class="col-12 mb-4">
                    <label for="" class="form-label">Tanggal Pengecekan</label>
                    <input type="date" class="form-control" name="tanggal_pengecekan" id="" value="{{ old('tanggal_pengecekan', @$pengecekan->tanggal_pengecekan) }}" required>
                    <div class="invalid-feedback">
                        Isi Tanggal Pengecekan.
                    </div>
                </div>
                <div class="col-12 mb-4">
                    <label for="" class="form-label">Jam Pengecekan</label>
                    <input type="time" class="form-control" name="jam" id="" value="{{ old('jam', @$pengecekan->jam) }}" required>
                    <div class="invalid-feedback">
                        Isi Jam Pengecekan.
                    </div>
                </div>
                {{-- <div class="col-12 mb-4">
                    <label for="" class="form-label">Upload Bukti Transfer</label>
                    <input type="file" class="form-control" name="bukti_trf" id="" value="" required>
                    <div class="invalid-feedback">
                        <span class="text-danger">*</span>Wajib upload bukti pembayaran.
                    </div>
                </div> --}}
                {{-- Status --}}
                <input type="hidden" class="form-control" name="status" id="" value="{{ old('status', @$pengecekan->status) }}">
                <div class="col-12 mb-4 text-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()
    </script>
@endpush