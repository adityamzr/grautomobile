@extends('layouts.home')

@section('content')
<div class="container mb-5">
    <div class="col my-3">
        <h1 class="h2 mb-3"><strong>Pengecekan</strong> Mobil</h1>
        <nav>
            <ol class="breadcrumb" style="background: #f3f6ff">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active"><a href="/userpengecekan">Pengecekan Mobil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Hasil Pengecekan</li>
            </ol>
        </nav>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row my-3 mx-4">
                <div class="col-5">Nama</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->user->nama }}</div>
            </div>
            <div class="row mb-3 mx-4">
                <div class="col-5">Nomor Handphone</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->user->nohp }}</div>
            </div>
            <div class="row mb-3 mx-4">
                <div class="col-5">Alamat</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->user->alamat }}</div>
            </div>
            <div class="row my-3 mx-4">
                <div class="col-5">Tanggal Pengecekan</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->tanggal_pengecekan }}</div>
            </div>
            {{-- <div class="row my-3 mx-4">
                <div class="col-5">Jumlah Tim Pengecekan</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->tim }}</div>
            </div> --}}
            <div class="row my-3 mx-4">
                <div class="col-5">Status</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->status }}</div>
            </div>
            <hr>
            <h4 class="my-3 mx-4 px-2"><strong>Hasil Pengecekan</strong></h4>
            <div class="row my-3 mx-4">
                <div class="col-5">Keterangan</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->hasilPengecekan->keterangan }}</div>
            </div>
            <div class="row my-3 mx-4">
                <div class="col-5">Deskripsi</div>
                <div class="col-1">:</div>
                <div class="col-6">{{ $hasil->hasilPengecekan->deskripsi }}</div>
            </div>
        </div>
    </div>
</div>
@endsection
