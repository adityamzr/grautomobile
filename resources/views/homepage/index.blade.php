@extends('layouts.home')

@section('content')
<!-- Main Slider Start -->
<!-- Bottom Bar Start -->
<div class="bottom-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="logo">
                    <a href="index.html">
                        <!-- <img src="{{ asset('/')}}home-asset/img/logo2.png" alt="Logo"> -->
                        <h3>Gunung Rahayu Automobile</h3>
                    </a>
                </div>
            </div>
            <div class="col-md-6" style="display: flex;">
                <div class="search">
                    <input type="text" placeholder="Search">
                </div>
                <div>
                    <button style="width: 40px;
                                height: 38px;
                                top: 1px;
                                right: 16px;
                                padding: 0;
                                border: none;
                                background: none;
                                color: #d23d21;
                                border-radius: 0 2px 2px 0;
                                border: 1px solid #d23d21;
                                margin-left: 12px;"><i class="fa fa-search"></i></button>
                </div>
            </div>
            @if (Auth::check())
            <div class="col-md-3">
                <div class="user">
                    <a href="/wishlist" class="btn wishlist">
                        <i class="fa fa-heart"></i>
                        <span>({{ $count }})</span>
                    </a>
                </div>
            </div>
            @endif
            
        </div>
    </div>
</div>
<!-- Bottom Bar End -->   
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <nav class="navbar bg-light">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/suv">SUV</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/sedan">Sedan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/double-cabin"></i>Double-cabin</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/pickup"></i>Pickup</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/diesel"></i>Diesel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/city-car"></i>City Car</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/produk/search/sport"></i>Sport</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6 col-lg-9">
                <div class="header-slider normal-slider">
                    @foreach ($mobil as $item)
                        <div class="header-slider-item">
                            <img src="{{ asset("storage/back/foto_mobil/$item->foto_mobil") }}" style="width: 100%; height: 400px;" alt="Slider Image" />
                        </div>    
                    @endforeach
                </div>
            </div>
            <!-- <div class="col-md-3">
                <div class="header-img">
                    <div class="img-item">
                        <img src="{{ asset('/')}}home-asset/img/category-1.jpg" />
                        <a class="img-text" href="">
                            <p>Some text goes here that describes the image</p>
                        </a>
                    </div>
                    <div class="img-item">
                        <img src="{{ asset('/')}}home-asset/img/category-2.jpg" />
                        <a class="img-text" href="">
                            <p>Some text goes here that describes the image</p>
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- Main Slider End -->      
<!-- Brand Start -->
<div class="brand">
    <div class="container-fluid">
        <div class="brand-slider">
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/toyota.png" style="width: 125px;" alt=""></div>
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/honda.png" style="width: 125px;" alt=""></div>
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/peugeot.png" style="width: 125px;" alt=""></div>
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/daihatsu.png" style="width: 125px;" alt=""></div>
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/bmw.png" style="width: 125px;" alt=""></div>
            <div class="brand-item"><img src="{{ asset('/')}}home-asset/img/mitsubishi.png" style="width: 125px;" alt=""></div>
        </div>
    </div>
</div>
<!-- Brand End -->      
  

<!-- Category Start-->
<!-- <div class="category">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="category-item ch-400">
                    <img src="{{ asset('/')}}home-asset/img/category-3.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="category-item ch-250">
                    <img src="{{ asset('/')}}home-asset/img/category-4.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
                <div class="category-item ch-150">
                    <img src="{{ asset('/')}}home-asset/img/category-5.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="category-item ch-150">
                    <img src="{{ asset('/')}}home-asset/img/category-6.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
                <div class="category-item ch-250">
                    <img src="{{ asset('/')}}home-asset/img/category-7.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="category-item ch-400">
                    <img src="{{ asset('/')}}home-asset/img/category-8.jpg" />
                    <a class="category-name" href="">
                        <p>Some text goes here that describes the image</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Category End-->       

<!-- Call to Action Start -->
<div class="call-to-action">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h1>Hubungi kami untuk pertanyaan</h1>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col">
                        <h1>Whatsapp:</h1>
                    </div>
                    <div class="col d-flex justify-content-start">
                        <a href="http://wa.me/6282118101906" target="_blank" style="text-decoration: none">082118101906</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Call to Action End -->        
<div class="col-12">
    <div class="mapouter">
        <div class="gmap_canvas">
            <iframe class="gmap_iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=600&amp;height=400&amp;hl=en&amp;q=Jl. Gunung Rahayu No.18 CD&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe><a href="https://www.kokagames.com/fnf-friday-night-funkin-mods/">FNF</a></div><style>.mapouter{position:relative;text-align:right;margin-right:0%;width:100%;height:400px;}.gmap_canvas {overflow:hidden;background:none!important;margin-right:0%;width:100%;height:400px;}.gmap_iframe {width:100%!important;height:500px!important;}</style></div>
</div>      

<!-- Review End -->       
<div class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 text-end">
                <div class="footer-widget px-5">
                    <h2>Get in Touch</h2>
                    <div class="contact-info">
                        <p><i class="fa fa-map-marker"></i>Jl. Gunung Rahayu No.18 CD</p>
                        <p><i class="fa fa-envelope"></i>amingfirdaus007@gmail.com</p>
                        <p><i class="fa fa-phone"></i>082118101906</p>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 text-start">
                <div class="footer-widget px-5">
                    <h2>Follow Us</h2>
                    <div class="contact-info">
                        <div class="social">
                            <a href="https://instagram.com/gunungrahayu.automobile"><i class="fab fa-instagram"></i></a> gunungrahayu.automobile
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer End --> 
@endsection
