@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <div class="row mb-3">
            <div class="col-6">
                <h1 class="h3 mb-3"><strong>Daftar</strong> User</h1>
            </div>
            <div class="col-6 text-end">
                <a href="/user/create" class="btn btn-primary"><i data-feather="plus"></i> Tambah</a>
            </div>
        </div>
        <table class="table table-bordered data-table" id="tabel">
            <thead>
                <tr>
                    <th class="col-auto">No</th>
                    <th class="col-auto">Nama</th>
                    <th class="col-auto">Email</th>
                    <th class="col-auto">Nomor Handphone</th>
                    <th class="col-auto">Alamat</th>
                    <th class="col-auto">Role</th>
                    <th class="col-auto text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->nohp }}</td>
                    <td>{{ $item->alamat }}</td>
                    <td>
                        @if ($item->role == 0) Admisi
                        @elseif($item->role == 1) Manager
                        @elseif($item->role == 2) Marketing Jual
                        @elseif($item->role == 3) Marketing Beli
                        @elseif($item->role == 4) Jasa Pengecekan
                        @else
                            User
                        @endif    
                    </td>
                    <td class="text-center">
                        <a href="{{ url('user/'.$item->id.'/edit') }}" class="btn btn-warning"><i data-feather="edit"></i> Edit</a>
                        <button type="button" class="btn btn-danger btn-hapus" data-id="{{ $item->id }}" data-nama="{{ $item->nama }}"><i data-feather="trash"></i> Hapus</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });
    

    $('.btn-hapus').on('click', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus User '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "user/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
</script>
@endpush