@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Form</strong> User</h1>
        <div class="card">
            <form action="{{ url('/user', @$user->id) }}" method="POST" class="needs-validation" novalidate>
                @csrf
                @if (!empty($user))
                    @method('PATCH')
                @endif
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-6">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" id="" value="{{ old('nama', @$user->nama) }}" required>
                            <div class="invalid-feedback">
                                Isi Nama.
                            </div>
                        </div>
                        <div class="col-6">
                            <label for="" class="form-label">Nomor Handphone</label>
                            <input type="number" class="form-control" name="nohp" id="" value="{{ old('nohp', @$user->nohp) }}" required>
                            <div class="invalid-feedback">
                                Isi Nomor Handphone.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-6">
                            <label for="" class="form-label">Email</label>
                            <input type="email" class="form-control" name="email" id="" value="{{ old('email', @$user->email) }}" required>
                            <div class="invalid-feedback">
                                Isi Email.
                            </div>
                        </div>
                        <div class="col-6">
                                <label for="" class="form-label">Password</label>
                                <input type="text" class="form-control" name="password" id="" value="" {{ old('password', @$user->password) != null ? '' : 'required'}}>
                                <span style="color: red">*</span><span>Isi minimal 8 karakter</span>
                                <div class="invalid-feedback">
                                    Isi Password.
                                </div>
                            {{-- @endif --}}
                            
                        </div>
                        <div class="col-12 my-3">
                            <label for="" class="form-label">Alamat</label>
                            <textarea class="form-control" name="alamat" id="" cols="10" rows="3" required>{{ old('alamat', @$user->alamat) }}</textarea>
                            <div class="invalid-feedback">
                                Isi Alamat.
                            </div>
                        </div>
                        {{-- @if (empty($user)) --}}
                        <div class="col-12 my-2">
                            <label for="" class="form-label">Role</label>
                            <select class="form-select" name="role" id="" required>
                                <option value="">--Pilih--</option>
                                <option value="0" {{ old('role', @$user->role) == '0' ? 'selected' : '' }}>Admisi</option>
                                <option value="1" {{ old('role', @$user->role) == '1' ? 'selected' : '' }}>Manager</option>
                                <option value="2" {{ old('role', @$user->role) == '2' ? 'selected' : '' }}>Marketing Jual</option>
                                <option value="3" {{ old('role', @$user->role) == '3' ? 'selected' : '' }}>Marketing Beli</option>
                                <option value="4" {{ old('role', @$user->role) == '4' ? 'selected' : '' }}>Jasa Pengecekan</option>
                                <option value="5" {{ old('role', @$user->role) == '5' ? 'selected' : '' }}>User</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih Role.
                            </div>
                        </div>
                        {{-- @endif --}}
                        
                    </div>
                    <div class="col-12 mb-4 text-end">
                        <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection

@push('script')
<script>
    (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
            event.preventDefault()
            event.stopPropagation()
            }

            form.classList.add('was-validated')
        }, false)
        })
    })()
</script>
@endpush