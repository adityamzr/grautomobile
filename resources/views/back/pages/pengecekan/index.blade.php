@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <div class="row mb-3">
            <div class="col-6">
                <h1 class="h3 mb-3"><strong>Daftar</strong> Pengecekan Mobil</h1>
            </div>
            @if (Auth::user()->role == 0 || Auth::user()->role == 1)
            <div class="col-6 text-end">
                <a href="/pengecekan/cetakPDF" class="btn btn-danger"><i data-feather="download"></i> Cetak PDF</a>
            </div>
            @endif
        </div>
        <table class="table table-bordered data-table" id="tabel">
            <thead>
                <tr>
                    <th class="col-auto">No</th>
                    <th class="col-auto">Nama Pemilik</th>
                    <th class="col-auto">Nomor Handphone</th>
                    <th class="col-auto">Alamat</th>
                    <th class="col-auto">Tanggal Pengecekan</th>
                    <th class="col-auto">Jam</th>
                    <th class="col-auto">Mekanik</th>
                    <th class="col-auto">Bukti Transfer</th>
                    <th class="col-auto">Status</th>
                    @if (Auth::user()->role != 0 && Auth::user()->role != 1)
                    <th class="col-auto text-center">Aksi</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($pengecekan as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->user->nama }}</td>
                    <td>{{ $item->user->nohp }}</td>
                    <td>{{ $item->user->alamat }}</td>
                    <td>{{ $item->tanggal_pengecekan }}</td>
                    <td>{{ $item->jam }}</td>
                    <td class="text-center">
                        @if ($item->status == 'Terima')
                            {{ $item->mekanik->nama_mekanik }}
                        @else
                            <span>-</span>
                        @endif    
                    </td>
                    <td class="text-center">
                        @if ($item->bukti_trf != null)
                        <button type="button" data-status="{{ $item->status }}" data-id="{{ $item->id_pengecekan }}" data-bukti="{{ $item->bukti_trf }}" data-bs-toggle="modal" data-bs-target="#buktiModal" class="btn btn-primary btn-bukti">Lihat Bukti</button>
                        @else
                            <span>-</span>
                        @endif
                    </td>
                    <td>
                        @if ($item->status == 'Pengajuan')
                            <span class="badge bg-primary">{{ $item->status }}</span>
                        @elseif($item->status == 'Terima')
                            <span class="badge bg-warning">Diproses</span>
                        @elseif($item->status == 'Tolak')
                            <span class="badge bg-danger">Ditolak</span>
                        @elseif($item->status == 'Menunggu')
                            <span class="badge bg-warning">Menunggu Pembayaran</span>
                        @elseif($item->status == 'Verifikasi')
                            <span class="badge bg-warning">Menunggu Verifikasi</span>
                        @else
                            <span class="badge bg-success">{{ $item->status }}</span>
                        @endif
                    </td>
                    @if (Auth::user()->role != 0 && Auth::user()->role != 1)
                    <td class="text-center">
                        @if ($item->status == 'Tolak')
                            <button type="button" id="btn-hapus" class="btn btn-danger" data-id="{{ $item->id_pengecekan }}" data-nama="{{ $item->user->nama }}"><i data-feather="trash"></i> Hapus</button>    
                        @elseif($item->status == 'Pengajuan')    
                            <button type="button" class="btn btn-danger btn-tolak" data-id="{{ $item->id_pengecekan }}" data-nama="{{ $item->user->nama }}"><i data-feather="x-circle"></i> Tolak</button>
                            <button type="button" class="btn btn-success btn-terima" data-id="{{ $item->id_pengecekan }}" data-nama="{{ $item->user->nama }}"><i data-feather="check-circle"></i> Terima</button>
                        @elseif($item->status == 'Selesai' || $item->status == 'Menunggu' || $item->status == 'Verifikasi')
                        
                        @else
                            <a href="{{ url('pengecekan/'.$item->id_pengecekan.'/edit') }}" class="btn btn-warning"><i data-feather="settings"></i> Proses</a>
                        @endif
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
<div class="modal fade" id="buktiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Bukti Transfer</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-center" id="bukti_trf">
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            @if (Auth::user()->role != 0 && Auth::user()->role != 1)
            <div id="formVerifikasi">

            </div>
            @endif
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });

    $('body').on('click', '.btn-tolak', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Tolak Jadwal Pengecekan Milik Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "pengecekan/"+id,
                type: 'PATCH',
                data: {
                "id": id,
                "status": 'Tolak',
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })

    $('body').on('click', '.btn-bukti', function(){
        const transfer = $(this).data('bukti')
        const id = $(this).data('id')
        const status = $(this).data('status')
        console.log(status)
        if(status === 'Verifikasi'){
            $('#formVerifikasi').html(`
            <form action="{{ url('pengecekan/verifikasi') }}" method="POST">
            @csrf
                <input type="hidden" name="id_pengecekan" id="pengecekan_id">
                <button type="submit" class="btn btn-primary">Verifikasi</button>
            </form>`)
        }
        $('#pengecekan_id').val(id)
        $('#bukti_trf').html(`<img src="{{ asset('storage/back/bukti_transfer/${transfer}') }}" width="400px" height="400px" alt="">`)
    })

    $('body').on('click', '.btn-terima', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Terima Jadwal Pengecekan Milik Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Terima',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "pengecekan/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Menunggu',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: data['message'],
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })

    $('#btn-hapus').on('click', function(){
        const id = $(this).data('id')
        const nama = $(this).data('id-nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Pengajuan Penjualan Mobil '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "userpengecekan/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
</script>
@endpush