@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Detail</strong> Pengecekan Mobil</h1>
        <div class="card col-8">
            <div class="card-body">
                <div class="row my-3 mx-4">
                    <div class="col-5">Nama Pemilik</div>
                    <div class="col-1">:</div>
                    <div class="col-6">{{ $pengecekan->user->nama }}</div>
                </div>
                <div class="row my-3 mx-4">
                    <div class="col-5">Nomor Handphone</div>
                    <div class="col-1">:</div>
                    <div class="col-6">{{ $pengecekan->user->nohp }}</div>
                </div>
                <div class="row my-3 mx-4">
                    <div class="col-5">Alamat</div>
                    <div class="col-1">:</div>
                    <div class="col-6">{{ $pengecekan->user->alamat }}</div>
                </div>
                <div class="row my-3 mx-4">
                    <div class="col-5">Mekanik</div>
                    <div class="col-1">:</div>
                    <div class="col-6">{{ $pengecekan->mekanik->nama_mekanik }}</div>
                </div>
                <div class="row my-3 mx-4">
                    <div class="col-5">Status</div>
                    <div class="col-1">:</div>
                    <div class="col-6">
                        @if ($pengecekan->status == 'Pengajuan')
                            <span class="badge bg-primary">{{ $pengecekan->status }}</span>
                        @elseif($pengecekan->status == 'Terima')
                            <span class="badge bg-warning">Diproses</span>
                        @elseif($item->status == 'Menunggu')
                            <span class="badge bg-warning">Menunggu Pembayaran</span>
                        @else
                            <span class="badge bg-success">Selesai</span>
                        @endif
                    </div>
                </div>
                <hr>
                <h3 class="mx-4 px-2">Hasil Pengecekan</h3>
                <form action="{{ url('/pengecekan') }}" method="POST" class="needs-validation" novalidate>
                @csrf
                <input type="hidden" value="{{ $pengecekan->id_pengecekan }}" name="id_pengecekan">
                <div class="row my-3 mx-4">
                    <div class="col-5">Keterangan</div>
                    <div class="col-1">:</div>
                    <div class="col-6">
                        <input type="text" class="form-control" id="keterangan" name="keterangan" value="" required/>    
                        <div class="invalid-feedback">
                            Isi keterangan.
                        </div>
                    </div>
                </div>
                <div class="row my-3 mx-4">
                    <div class="col-5">Deskripsi</div>
                    <div class="col-1">:</div>
                    <div class="col-6">
                        <textarea class="form-control" cols="10" rows="5" id="deskripsi" name="deskripsi" value="" required></textarea>
                        <div class="invalid-feedback">
                            Isi deskripsi.
                        </div>
                    </div>
                </div>
                {{-- <input type="hidden" name="jmlHasil" id="jmlHasil" value="{{ old('jmlHasil', ($hasil->count() == 0)? '1' : $hasil->count()) }}"> --}}
                {{-- <div id="containerHasil">
                    @forelse ($hasil as $item)
                    <div id="box{{ $loop->iteration }}">
                        <div class="row my-3 mx-4">
                            <div class="col-5">Keterangan</div>
                            <div class="col-1">:</div>
                            <div class="col-6">
                                <input type="text" class="form-control" id="keterangan" name="keterangan[]" value="" required/>    
                                <div class="invalid-feedback">
                                    Isi keterangan.
                                </div>
                            </div>
                        </div>
                        <div class="row my-3 mx-4">
                            <div class="col-5">Deskripsi</div>
                            <div class="col-1">:</div>
                            <div class="col-6">
                                <textarea class="form-control" cols="10" rows="5" id="deskripsi" name="deskripsi[]" value="" required></textarea>
                                <div class="invalid-feedback">
                                    Isi deskripsi.
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                    @for ($i = 1; $i <= old('jmlHasil', '1'); $i++)
                    <div id="box{{ $i }}">
                        <div class="row my-3 mx-4">
                            <div class="col-5">Keterangan</div>
                            <div class="col-1">:</div>
                            <div class="col-6">
                                <input type="text" class="form-control" id="keterangan" name="keterangan[]" value="" required/>    
                                <div class="invalid-feedback">
                                    Isi keterangan.
                                </div>
                            </div>
                        </div>
                        <div class="row my-3 mx-4">
                            <div class="col-5">Deskripsi</div>
                            <div class="col-1">:</div>
                            <div class="col-6">
                                <textarea class="form-control" cols="10" rows="5" id="deskripsi" name="deskripsi[]" value="" required></textarea>
                                <div class="invalid-feedback">
                                    Isi deskripsi.
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor    
                    @endforelse
                    
                </div> --}}
                    
                <div class="col-11 mt-5 mb-4 mx-4">
                    <div class="row justify-content-between">
                        <div class="col-3">
                            <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                        </div>
                        {{-- <div class="col-8 text-end">
                            <button type="button" id="btn-tambah" class="btn btn-success btn-lg"><i data-feather="plus"></i> Tambah Keterangan</button>
                            <button type="button" id="btn-kurang" class="btn btn-danger btn-lg"><i data-feather="minus"></i> Kurangi Keterangan</button>
                        </div> --}}
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        // $('#btn-tambah').on('click', function(){
        //     var i = $('#jmlHasil').val(); 
        //     i++; 
        //     $('.hasil').append(`
        //     <div class="row my-3 mx-4">
        //         <div class="col-5">Keterangan</div>
        //         <div class="col-1">:</div>
        //         <div class="col-6">
        //             <input type="text" class="form-control" id="keterangan" name="keterangan[]" value="" required/>    
        //             <div class="invalid-feedback">
        //                 Isi keterangan.
        //             </div>
        //         </div>
        //     </div>
        //     <div class="row my-3 mx-4">
        //         <div class="col-5">Deskripsi</div>
        //         <div class="col-1">:</div>
        //         <div class="col-6">
        //             <textarea class="form-control" cols="10" rows="5" id="deskripsi" name="deskripsi[]" value="" required></textarea>
        //             <div class="invalid-feedback">
        //                 Isi deskripsi.
        //             </div>
        //         </div>
        //     </div>`);

        //     $('#jmlHasil').val(i);
        // })

        // $('#btn-kurang').on('click', function(){
        //     alert('zfs')
        // })
        
    </script>
@endpush