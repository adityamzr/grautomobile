<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap.css"> --}}
    <title>Laporan Pengecekan</title>
</head>
<style>
    .section{

    }
    .head{
        text-align: center;
        width: 100%;
    }
    .container{
        display: flex;
        justify-content: center;
    }
    table{
        border: 1px solid black;
        /* border-collapse: separate; */
        border-spacing: 0px;
    }
    td{
        border: 1px solid black;
        padding: 12px;
        border-spacing: 0px;
    }
    hr{
        height: 1px;
        background-color: black;
        margin-bottom: 8px;
    }
</style>
<body>
    <div class="section">
        <div class="head">
            <h1>Laporan Pengecekan</h1>
            <h3>Gunung Rahayu Automobile</h3>
            <h3>Tanggal {{ $tanggal }}</h3>
        </div>
        <hr>
        <div class="container">
            <table>
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Tanggal</td>
                        <td>Jam</td>
                        <td>Nama Pemilik</td>
                        <td>Nomor HP</td>
                        <td>Alamat</td>
                        <td>Keterangan</td>
                        <td>Deskripsi</td>
                        <td>Status</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pengecekan as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->tanggal_pengecekan }}</td>
                            <td>{{ $item->jam }}</td>
                            <td>{{ $item->user->nama }}</td>
                            <td>{{ $item->user->nohp }}</td>
                            <td>{{ $item->user->alamat }}</td>
                            <td>{{ $item->hasilPengecekan->keterangan }}</td>
                            <td>{{ $item->hasilPengecekan->deskripsi }}</td>
                            <td>{{ $item->status }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>