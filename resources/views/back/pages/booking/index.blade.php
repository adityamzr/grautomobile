@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <div class="row mb-3">
            <div class="col-6">
                <h1 class="h3 mb-3"><strong>Daftar</strong> Booking</h1>
            </div>
        </div>
        <table class="table table-bordered data-table" id="tabel">
            <thead>
                <tr>
                    <th class="col-auto">No</th>
                    <th class="col-auto">Nama</th>
                    <th class="col-auto">Nomor Handphone</th>
                    <th class="col-auto">Alamat</th>
                    <th class="col-auto">Mobil</th>
                    <th class="col-auto">Merk</th>
                    <th class="col-auto">Tahun</th>
                    <th class="col-auto">Harga Mobil</th>
                    <th class="col-auto">Status</th>
                    <th class="col-auto">Bukti</th>
                    <th class="col-auto text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    function titik($angka){
                        $hasil_rupiah = "" . number_format($angka,0,'','.');

                        return $hasil_rupiah;
                    }
                @endphp
                @foreach ($booking as $item)
                @php
                    $foto = $item->foto_mobil;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->user->nama }}</td>
                    <td>{{ $item->user->nohp }}</td>
                    <td>{{ $item->user->alamat }}</td>
                    <td>{{ $item->mobil->nama_mobil }}</td>
                    <td>{{ $item->mobil->merk_mobil }}</td>
                    <td>{{ $item->mobil->tahun_produksi }}</td>
                    <td>Rp{{ titik($item->mobil->harga_mobil) }}</td>
                    <td>{{ $item->status }}</td>
                    <td><button type="button" data-status="{{ $item->status }}" data-id="{{ $item->id_booking }}" data-bukti="{{ $item->bukti_book }}" data-bs-toggle="modal" data-bs-target="#buktiModal" class="btn btn-primary btn-bukti">Lihat</button></td>
                    <td class="text-center">
                        @if ($item->status == 'Ditolak')
                          <button type="button" class="btn btn-danger btn-hapus" data-id-booking="{{ $item->id_booking }}" data-nama="{{ $item->user->nama }}"><i data-feather="trash"></i> Hapus</button>
                        @elseif($item->status == 'Booking')    
                          <button type="button" class="btn btn-danger btn-tolak" data-id="{{ $item->id_booking }}" data-nama="{{ $item->user->nama }}"><i data-feather="x-circle"></i> Tolak</button>
                          <button type="button" class="btn btn-success btn-terima" data-id="{{ $item->id_booking }}" data-nama="{{ $item->user->nama }}"><i data-feather="check-circle"></i> Terima</button>
                        @else
                          <a href="http://wa.me/{{ $item->user->nohp }}" target="_blank" class="btn btn-success"><i class="fab fa-whatsapp"></i> Kontak Pembeli</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
<div class="modal fade" id="buktiModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Bukti Transfer</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
              <div class="d-flex justify-content-center" id="bukti_book">
              </div>
          </div>
          <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <div id="formVerifikasi">

          </div>
          </div>
      </div>
  </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });

    $('body').on('click', '.btn-bukti', function(){
        const transfer = $(this).data('bukti')
        const id = $(this).data('id')
        $('#booking_id').val(id)
        $('#bukti_book').html(`<img src="{{ asset('storage/back/bukti_booking/${transfer}') }}" width="400px" height="400px" alt="">`)
    })

    $('body').on('click', '.btn-terima', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Terima Booking Mobil Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Terima',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "booking/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Booked',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: data['message'],
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })

    $('body').on('click', '.btn-tolak', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Tolak Booking Mobil Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "booking/"+id,
                type: 'PATCH',
                data: {
                "id": id,
                "status": 'Ditolak',
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
    

    $('.btn-hapus').on('click', function(){
        const id = $(this).data('id-booking')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Booking Mobil Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "booking/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
</script>
@endpush