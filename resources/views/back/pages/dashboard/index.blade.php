@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Hi,</strong> Admin</h1>
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-body px-4">
                        <div class="row">
                            <div class="col mt-0">
                                <div class="card-title">Mobil Tersedia</div>
                            </div>
                            <div class="col-auto">
                                <div class="stat text-primary">
                                    <i class="fas fa-car"></i>
                                </div>
                            </div>
                        </div>
                        <h1 class="mt-1 mb-3">{{ $stok }} Unit</h1>
                        <div class="mb-0">
                            <span class="text-muted">Per Tanggal {{ $tanggal }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-body px-4">
                        <div class="row">
                            <div class="col mt-0">
                                <div class="card-title">Mobil Dibeli</div>
                            </div>
                            <div class="col-auto">
                                <div class="stat text-primary">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                            </div>
                        </div>
                        <h1 class="mt-1 mb-3">{{ $deal }} Unit</h1>
                        <div class="mb-0">
                            <span class="text-muted">Per Tanggal {{ $tanggal }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card">
                    <div class="card-body px-4">
                        <div class="row">
                            <div class="col mt-0">
                                <div class="card-title">Pengecekan Mobil Selesai</div>
                            </div>
                            <div class="col-auto">
                                <div class="stat text-primary">
                                    <i class="fas fa-calendar-check"></i>
                                </div>
                            </div>
                        </div>
                        <h1 class="mt-1 mb-3">{{ $cek }} Unit</h1>
                        <div class="mb-0">
                            <span class="text-muted">Per Tanggal {{ $tanggal }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection