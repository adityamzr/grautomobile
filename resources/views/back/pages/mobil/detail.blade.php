@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Detail</strong> Mobil</h1>
        <div class="row">
            <div class="card col-8">
                <div class="card-body">
                    <div class="row my-3 mx-4">
                        <div class="col-5">Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->nama_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-5">Merk Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->merk_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-5">Bensin</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->bensin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kilometer</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->kilometer }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Transmisi</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->transmisi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kepemilikan</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->kepemilikan }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kapasitas Mesin</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->kapasitas_mesin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Pajak</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->pajak }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Deskripsi</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $mobil->deskripsi }}</div>
                    </div>
                    @php
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');
    
                            return $hasil_rupiah;
                        }
                    @endphp
                    <div class="row my-3 mx-4">
                        <div class="col-5">Harga Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-6">Rp{{ titik($mobil->harga_mobil) }}</div>
                    </div>
                </div>
                @if ($beli)
                <div class="card-header pb-0">
                    <h4 class="mx-4">Calon Pembeli</h4>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Nama</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $beli->user->nama }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Nomor Hp</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $beli->user->nohp }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Tanggal</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $beli->tanggal }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Jam</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $beli->jam }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Status</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $beli->status }}</div>
                    </div>
                    <div class="row mt-2 mb-4">
                        @if ($beli->status == 'Pengajuan')
                        <div class="col-6">
                            <button type="button" data-id-beli="{{ $beli->id_beli }}" data-nama="{{ $beli->user->nama }}" class="btn btn-danger w-100 btn-tolak"><i data-feather="x-circle"></i> Tolak</button>
                        </div>
                        <div class="col-6">
                            <button type="button" data-id-beli="{{ $beli->id_beli }}" data-nama="{{ $beli->user->nama }}" class="btn btn-success w-100 btn-terima"><i data-feather="check-circle"></i> Terima</button>
                        </div>
                        @elseif($beli->status == 'Nego')
                        <div class="col-6">
                            <button type="button" data-id-beli="{{ $beli->id_beli }}" data-nama="{{ $beli->user->nama }}" class="btn btn-danger w-100 btn-batal"><i data-feather="x-circle"></i> Batal</button>
                        </div>
                        <div class="col-6">
                            <button type="button" data-id-beli="{{ $beli->id_beli }}" data-nama="{{ $beli->user->nama }}" class="btn btn-success w-100 btn-deal"><i data-feather="check-circle"></i> Deal</button>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
            </div>
            <div class="col-4">
                <div class="card p-2">
                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                        </div>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $mobil->nama_mobil }}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil2") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $mobil->nama_mobil }}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset("storage/back/foto_mobil/$mobil->foto_mobil3") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $mobil->nama_mobil }}">
                          </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@push('script')
    <script>
    $('body').on('click', '.btn-tolak', function(){
        const id = $(this).data('id-beli')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Tolak Pengajuan Beli Mobil Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "belimobil/"+id,
                type: 'PATCH',
                data: {
                "id": id,
                "status": 'Tolak',
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: 'Berhasil menolak pengajuan',
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })    

    $('body').on('click', '.btn-terima', function(){
        const id = $(this).data('id-beli')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Terima Pengajuan Beli Mobil Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Terima',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "belimobil/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Nego',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: 'Berhasil menerima pengajuan',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })

    $('body').on('click', '.btn-deal', function(){
        const id = $(this).data('id-beli')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Deal dengan pembelian Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Deal',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "belimobil/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Deal',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: 'Berhasil menjual mobil',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })

    $('body').on('click', '.btn-batal', function(){
        const id = $(this).data('id-beli')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Batal melakukan transaksi dari Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Ya',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "belimobil/"+id,
                type: 'PATCH',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: 'Batal menjual mobil',
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    }) 
    </script>    

@endpush