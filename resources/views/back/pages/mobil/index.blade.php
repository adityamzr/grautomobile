@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <div class="row mb-3">
            <div class="col-6">
                <h1 class="h3 mb-3"><strong>Daftar</strong> Mobil</h1>
            </div>
            <div class="col-6 text-end">
                @if (Auth::user()->role == 0 || Auth::user()->role == 1)
                <a href="/mobil/cetakPDF" class="btn btn-danger"><i data-feather="download"></i> Cetak PDF</a>
                @endif
                @if (Auth::user()->role != 0 && Auth::user()->role != 1)
                <a href="/mobil/create" class="btn btn-primary"><i data-feather="plus"></i> Tambah</a>
                @endif
            </div>
        </div>
        <table class="table table-bordered data-table" id="tabel">
            <thead>
                <tr>
                    <th class="col-auto">No</th>
                    <th class="col-auto">Foto</th>
                    <th class="col-auto">Nama</th>
                    <th class="col-auto">Merk</th>
                    <th class="col-auto">Transmisi</th>
                    <th class="col-auto">Harga</th>
                    <th class="col-auto">Status</th>
                    <th class="col-auto text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    function titik($angka){
                        $hasil_rupiah = "" . number_format($angka,0,'','.');

                        return $hasil_rupiah;
                    }
                @endphp
                @foreach ($mobil as $item)
                @php
                    $foto = $item->foto_mobil;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td class="text-center"><img src="{{ asset("storage/back/foto_mobil/$foto") }}" width="50%" height="60px" alt="Foto Mobil"></td>
                    <td>{{ $item->nama_mobil }}</td>
                    <td>{{ $item->merk_mobil }}</td>
                    <td>{{ $item->transmisi }}</td>
                    <td>Rp{{ titik($item->harga_mobil) }}</td>
                    <td>{{ $item->status }}</td>
                    <td class="text-center">
                        <a href="{{ url('mobil/'.$item->id_mobil) }}" class="btn btn-info"><i data-feather="eye"></i> Detail</a>
                        @if (Auth::user()->role != 0 && Auth::user()->role != 1)
                        <a href="{{ url('mobil/'.$item->id_mobil.'/edit') }}" class="btn btn-warning"><i data-feather="edit"></i> Edit</a>
                        <button type="button" class="btn btn-danger btn-hapus" data-id-mobil="{{ $item->id_mobil }}" data-nama-mobil="{{ $item->nama_mobil }}"><i data-feather="trash"></i> Hapus</button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });
    

    $('.btn-hapus').on('click', function(){
        const id = $(this).data('id-mobil')
        const nama = $(this).data('nama-mobil')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Hapus Mobil '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Hapus',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "mobil/"+id,
                type: 'DELETE',
                data: {
                "id": id,
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })
</script>
@endpush