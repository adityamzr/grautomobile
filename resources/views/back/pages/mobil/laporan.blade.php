<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap.css"> --}}
    <title>Laporan Mobil</title>
</head>
<style>
    .section{

    }
    .head{
        text-align: center;
        width: 100%;
    }
    .container{
        display: flex;
        justify-content: center;
    }
    table{
        border: 1px solid black;
        /* border-collapse: separate; */
        border-spacing: 0px;
    }
    td{
        border: 1px solid black;
        padding: 12px;
        border-spacing: 0px;
    }
    hr{
        height: 1px;
        background-color: black;
        margin-bottom: 8px;
    }
</style>
<body>
    <div class="section">
        <div class="head">
            <h1>Laporan Mobil</h1>
            <h3>Gunung Rahayu Automobile</h3>
            <h3>Tanggal {{ $tanggal }}</h3>
        </div>
        <hr>
        <div class="container">
            <table>
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama</td>
                        <td>Merk</td>
                        <td>Model</td>
                        <td>Tipe</td>
                        <td>Tahun</td>
                        <td>Bensin</td>
                        <td>Kilometer</td>
                        <td>Transmisi</td>
                        <td>Pajak</td>
                        <td>Harga</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');
        
                            return $hasil_rupiah;
                        }
                    @endphp
                    @foreach ($mobil as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->nama_mobil }}</td>
                            <td>{{ $item->merk_mobil }}</td>
                            <td>{{ $item->model }}</td>
                            <td>{{ $item->tipe }}</td>
                            <td>{{ $item->tahun_produksi }}</td>
                            <td>{{ $item->bensin }}</td>
                            <td>{{ titik($item->kilometer) }}</td>
                            <td>{{ $item->transmisi }}</td>
                            <td>{{ $item->pajak }}</td>
                            <td>Rp{{ titik($item->harga_mobil) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>