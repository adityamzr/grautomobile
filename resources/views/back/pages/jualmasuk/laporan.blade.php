<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('app-assets') }}/css/bootstrap.css"> --}}
    <title>Laporan Jual Masuk</title>
</head>
<style>
    .section{

    }
    .head{
        text-align: center;
        width: 100%;
    }
    .container{
        display: flex;
        justify-content: center;
    }
    table{
        border: 1px solid black;
        /* border-collapse: separate; */
        border-spacing: 0px;
    }
    td{
        border: 1px solid black;
        padding: 12px;
        border-spacing: 0px;
    }
    hr{
        height: 1px;
        background-color: black;
        margin-bottom: 8px;
    }
</style>
<body>
    <div class="section">
        <div class="head">
            <h1>Laporan Jual Masuk</h1>
            <h3>Gunung Rahayu Automobile</h3>
            <h3>Tanggal {{ $tanggal }}</h3>
        </div>
        <hr>
        <div class="container">
            <table>
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Pemilik</td>
                        <td>Nomor HP</td>
                        <td>Alamat</td>
                        <td>Mobil</td>
                        <td>Merk</td>
                        <td>Tahun</td>
                        <td>Transmisi</td>
                        <td>Harga</td>
                        <td>Status</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        function titik($angka){
                            $hasil_rupiah = "" . number_format($angka,0,'','.');
        
                            return $hasil_rupiah;
                        }
                    @endphp
                    @foreach ($jual as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->user->nama }}</td>
                            <td>{{ $item->user->nohp }}</td>
                            <td>{{ $item->user->alamat }}</td>
                            <td>{{ $item->mobil->nama_mobil }}</td>
                            <td>{{ $item->mobil->merk_mobil }}</td>
                            <td>{{ $item->mobil->tahun_produksi }}</td>
                            <td>{{ $item->mobil->transmisi }}</td>
                            <td>Rp{{ titik($item->mobil->harga_mobil) }}</td>
                            <td>{{ $item->mobil->status }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>