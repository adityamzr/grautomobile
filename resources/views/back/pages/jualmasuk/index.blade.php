@extends('back.layouts.app')

@section('content')
<main class="content">
    <div class="container-fluid p-0">
        <div class="row mb-3">
            <div class="col-6">
                <h1 class="h3 mb-3"><strong>Daftar</strong> Penjualan Mobil</h1>
            </div>
            @if (Auth::user()->role == 0 || Auth::user()->role == 1)
            <div class="col-6 text-end">
                <a href="/jualmasuk/cetakPDF" class="btn btn-danger"><i data-feather="download"></i> Cetak PDF</a>
            </div>
            @endif
        </div>
        <table class="table table-bordered data-table" id="tabel">
            <thead>
                <tr>
                    <th class="col-auto">No</th>
                    <th class="col-auto">Foto Mobil</th>
                    <th class="col-auto">Nama Pemilik</th>
                    <th class="col-auto">Nomor HP</th>
                    <th class="col-auto">Mobil</th>
                    <th class="col-auto">Merk</th>
                    <th class="col-auto">Transmisi</th>
                    <th class="col-auto">Harga</th>
                    <th class="col-auto">Status</th>
                    <th class="col-auto text-center">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @php
                    function titik($angka){
                        $hasil_rupiah = "" . number_format($angka,0,'','.');

                        return $hasil_rupiah;
                    }
                    @endphp
                @foreach ($jual as $item)
                @php
                    $foto = $item->mobil->foto_mobil;
                @endphp
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td class="text-center"><img src="{{ asset("storage/back/foto_mobil/$foto") }}" width="50%" height="60px" alt="Foto Mobil"></td>
                    <td>{{ $item->user->nama }}</td>
                    <td>{{ $item->user->nohp }}</td>
                    <td>{{ $item->mobil->nama_mobil }}</td>
                    <td>{{ $item->mobil->merk_mobil }}</td>
                    <td>{{ $item->mobil->transmisi }}</td>
                    <td>Rp{{ titik($item->mobil->harga_mobil) }}</td>
                    <td>
                        @if ($item->mobil->status == 'Pengajuan')
                            <span class="badge bg-primary">{{ $item->mobil->status }}</span>
                        @elseif($item->mobil->status == 'Nego')
                            <span class="badge bg-warning">{{ $item->mobil->status }}</span>
                        @elseif($item->mobil->status == 'Tolak')
                            <span class="badge bg-danger">{{ $item->mobil->status }}</span>
                        @else
                            <span class="badge bg-success">{{ $item->mobil->status }}</span>
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ url('jualmasuk/'.$item->id_mobil. '/edit') }}" class="btn btn-info mb-1"><i data-feather="eye"></i> Detail</a><br>
                        @if (Auth::user()->role != 0 && Auth::user()->role != 1)
                            @if ($item->mobil->status == 'Tolak')
                                <button type="button" class="btn btn-danger btn-hapus" data-id="{{ $item->id_jual }}" data-nama="{{ $item->user->nama }}"><i data-feather="trash"></i> Hapus</button>    
                            @elseif($item->mobil->status == 'Pengajuan')    
                                <button type="button" class="btn btn-danger btn-tolak mb-1" data-id="{{ $item->mobil->id_mobil }}" data-nama="{{ $item->user->nama }}"><i data-feather="x-circle"></i> Tolak</button><br>
                                <button type="button" class="btn btn-success btn-terima" data-id="{{ $item->mobil->id_mobil }}" data-nama="{{ $item->user->nama }}"><i data-feather="check-circle"></i> Terima</button>
                            @elseif($item->mobil->status == 'Nego' && $item->alamat != null)
                                <button type="button" class="btn btn-danger btn-tolak mb-1" data-id="{{ $item->mobil->id_mobil }}" data-nama="{{ $item->user->nama }}"><i data-feather="x-circle"></i> Tolak</button><br>
                                <button type="button" class="btn btn-success btn-deal" data-id="{{ $item->mobil->id_mobil }}" data-nama="{{ $item->user->nama }}"><i data-feather="check-circle"></i> Deal</button>
                            @else
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $('#tabel').DataTable();
    });

    $('body').on('click', '.btn-tolak', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')

        Swal.fire({
            title: 'Peringatan!',
            text: 'Tolak Pengajuan Jual Mobil Pak/Bu '+nama+'?',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "jualmasuk/"+id,
                type: 'PATCH',
                data: {
                "id": id,
                "status": 'Tolak',
                "_token": "{{ csrf_token() }}"
                },
                success: function(data){
                    Swal.fire({
                    position: 'top-center',
                    icon: 'success',
                    title: 'Success!',
                    text: 'Berhasil menolak pengajuan',
                    showConfirmButton: false,
                    timer: 1500
                    })
                }
            });
        } else if (result.isDenied) {
            Swal.fire('Data gagal dihapus!', '', 'info')
        }
    })
    })

    $('body').on('click', '.btn-terima', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Terima Pengajuan Jual Mobil Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Terima',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "jualmasuk/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Nego',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: 'Berhasil menerima pengajuan',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })
    
    $('body').on('click', '.btn-deal', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Deal dengan penjualan mobil dari Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#1CBB8C',
                confirmButtonText: 'Deal',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "jualmasuk/"+id,
                    type: 'PATCH',
                    data: {
                    "id": id,
                    "status": 'Deal',
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: 'Berhasil membeli mobil',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })

    $('body').on('click', '.btn-hapus', function(){
        const id = $(this).data('id')
        const nama = $(this).data('nama')
            Swal.fire({
                title: 'Peringatan!',
                text: 'Hapus Pengajuan yang sudah ditolak dari Pak/Bu '+nama+'?',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                confirmButtonText: 'Hapus',
            }).then((result) => {
                if (result.isConfirmed) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "jualmasuk/"+id,
                    type: 'DELETE',
                    data: {
                    "id": id,
                    "_token": "{{ csrf_token() }}"
                    },
                    success: function(data){
                        Swal.fire({
                        position: 'top-center',
                        icon: 'success',
                        title: 'Success!',
                        text: 'Berhasil menghapus data',
                        showConfirmButton: false,
                        timer: 1500
                        })
                    }
                });
            } else if (result.isDenied) {
                Swal.fire('Data gagal dihapus!', '', 'info')
            }
        })
    })
</script>
@endpush