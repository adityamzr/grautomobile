@extends('back.layouts.app')

@section('content')
@php
    function titik($angka){
        $hasil_rupiah = "" . number_format($angka,0,'','.');

        return $hasil_rupiah;
    }
@endphp
<main class="content">
    <div class="container-fluid p-0">
        <h1 class="h3 mb-3"><strong>Detail</strong> Penjualan Mobil</h1>
        <div class="row">
            <div class="card col-8">
                <div class="card-body">
                    <h4>Jadwal Negosiasi</h6>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Alamat</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->alamat }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Tanggal</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->tanggal }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Jam</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->jam }}</div>
                    </div>
                    <hr>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Nama Pemilik</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->user->nama }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Nomor Handphone</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->user->nohp }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Alamat</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->user->alamat }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->nama_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-5">Merk Mobil</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->merk_mobil }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-5">Model Unit</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->model }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Tipe</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->tipe }}</div>
                    </div>
                    <div class="row mb-3 mx-4">
                        <div class="col-5">Bensin</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->bensin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kilometer</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->kilometer }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Transmisi</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->transmisi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kepemilikan</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->kepemilikan }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Kapasitas Mesin</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->kapasitas_mesin }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Pajak</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->pajak }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Tahun Produksi</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->tahun_produksi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Deskripsi</div>
                        <div class="col-1">:</div>
                        <div class="col-6">{{ $jual->mobil->deskripsi }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Harga Penawaran</div>
                        <div class="col-1">:</div>
                        <div class="col-6">Rp{{ titik($jual->harga_penawaran) }}</div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Status</div>
                        <div class="col-1">:</div>
                        <div class="col-6">
                            @if ($jual->mobil->status == 'Pengajuan')
                                <span class="badge bg-primary">{{ $jual->mobil->status }}</span>
                            @elseif($jual->mobil->status == 'Nego')
                                <span class="badge bg-warning">{{ $jual->mobil->status }}</span>
                            @else
                                <span class="badge bg-success">{{ $jual->mobil->status }}</span>
                            @endif
                        </div>
                    </div>
                    {{-- <hr>
                    <h3 class="mx-4 px-2">Negosiasi</h3>
                    <form action="{{ url('/jualmasuk', @$jual->id_mobil) }}" method="POST" class="needs-validation" novalidate>
                    @csrf
                    @if (!empty($jual))
                        @method('PATCH')
                    @endif
                    <div class="row my-3 mx-4">
                        <div class="col-5">Ubah Status</div>
                        <div class="col-1">:</div>
                        <div class="col-6">
                            @if ($jual->mobil->status == 'Deal')
                            <select class="form-select" name="status" id="ubah_status" required>
                                <option value="">--Pilih--</option>
                                <option value="Nego" {{ old('ubah_status', @$jual->mobil->status) == 'Deal' ? 'selected' : '' }}>Deal</option>
                                <option value="Tersedia">Tersedia</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih status.
                            </div>    
                            @else
                            <select class="form-select" name="status" id="ubah_status" required>
                                <option value="">--Pilih--</option>
                                <option value="Nego" {{ old('ubah_status', @$jual->mobil->status) == 'Nego' ? 'selected' : '' }}>Nego</option>
                                <option value="Deal">Deal</option>
                                <option value="Tolak">Tolak</option>
                            </select>
                            <div class="invalid-feedback">
                                Pilih status negosiasi.
                            </div>
                        </div>
                    </div>
                    <div class="row my-3 mx-4">
                        <div class="col-5">Harga Nego</div>
                        <div class="col-1">:</div>
                        <div class="col-6">
                            <input type="number" class="form-control" id="harga_nego" name="harga_mobil" value="{{ old('harga_mobil', @$jual->harga_penawaran) }}" required/>    
                            <div class="invalid-feedback">
                                Isi harga nego.
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-8 my-4 mx-4 px-2">
                        <button type="submit" class="btn btn-primary btn-lg">Simpan</button>
                    </div>
                    </form> --}}
                </div>
            </div>
            <div class="col-4">
                <div class="card p-2">
                    @php
                        $foto = $jual->mobil->foto_mobil;
                        $foto2 = $jual->mobil->foto_mobil2;
                        $foto3 = $jual->mobil->foto_mobil3;
                        $foto4 = $jual->mobil->foto_mobil4;
                    @endphp

                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Slide 4"></button>
                        </div>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="{{ asset("storage/back/foto_mobil/$foto") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $jual->mobil->nama_mobil }}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset("storage/back/foto_mobil/$foto2") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $jual->mobil->nama_mobil }}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset("storage/back/foto_mobil/$foto3") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $jual->mobil->nama_mobil }}">
                          </div>
                          <div class="carousel-item">
                            <img src="{{ asset("storage/back/foto_mobil/$foto4") }}" class="d-block w-100" height="250px" alt="Foto Mobil {{ $jual->mobil->nama_mobil }}">
                          </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="visually-hidden">Next</span>
                        </button>
                      </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@push('script')
    <script>
        (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
            })
        })()

        var  nego = $('#ubah_status').find('option:selected').val();
        if(nego  === 'Nego'){
                $('#harga_nego').prop('readonly', false)
            }else{
                $('#harga_nego').prop('readonly',true)
            }

        $('#ubah_status').on('change', function(){
            const status = $(this).val()
            if(status  === 'Nego'){
                $('#harga_nego').prop('readonly', false)
            }else{
                $('#harga_nego').prop('readonly',true)
            }
        })

        
    </script>
@endpush