@php
    $role = Auth::user()->role;
@endphp
<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="index.html">
            <span class="align-middle">
                @if ($role == 0) Admisi GRA
                @elseif($role == 1) Manager GRA
                @elseif($role == 2) Marketing Jual GRA
                @elseif($role == 3) Marketing Beli GRA
                @elseif($role == 4) Jasa Pengecekan GRA
                @endif
            </span>
        </a>

        <ul class="sidebar-nav">
            <li class="sidebar-header">
                Pages
            </li>
            <li class="sidebar-item {{ $title == 'dashboard' ? 'active' : '' }}">
                <a class="sidebar-link" href="/dashboard">
                    <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                </a>
            </li>
            @if ($role == 0 || $role == 1 || $role == 2)
            <li class="sidebar-item {{ $title == 'mobil' ? 'active' : '' }}">
                <a class="sidebar-link" href="/mobil">
                    <i class="align-middle" data-feather="target"></i> <span class="align-middle">Mobil</span>
                </a>
            </li>
            @endif
            {{-- @if ($role == 0 || $role == 1)
            <li class="sidebar-item {{ $title == 'booking' ? 'active' : '' }}">
                <a class="sidebar-link" href="/booking">
                    <i class="align-middle" data-feather="bookmark"></i> <span class="align-middle">Booking</span>
                </a>
            </li>
            @endif --}}
            @if ($role == 0 || $role == 1 || $role == 3)
            <li class="sidebar-item {{ $title == 'jualmasuk' ? 'active' : '' }}">
                <a class="sidebar-link" href="/jualmasuk">
                    <i class="align-middle" data-feather="log-in"></i> <span class="align-middle">Jual Masuk</span>
                </a>
            </li>
            @endif
            @if ($role == 0 || $role == 1 || $role == 4)
            <li class="sidebar-item {{ $title == 'pengecekan' ? 'active' : '' }}">
                <a class="sidebar-link" href="/pengecekan">
                    <i class="align-middle" data-feather="check-circle"></i> <span class="align-middle">Pengecekan</span>
                </a>
            </li>
            @endif
            @if ($role == 0)
            <li class="sidebar-item {{ $title == 'user' ? 'active' : '' }}">
                <a class="sidebar-link" href="/user">
                    <i class="align-middle" data-feather="user"></i> <span class="align-middle">User</span>
                </a>
            </li>
            @endif
        </ul>
    </div>
</nav>