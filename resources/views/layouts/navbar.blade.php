<!-- Nav Bar Start -->
<div class="nav">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a href="#" class="navbar-brand">MENU</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                    <a href="/" class="nav-item nav-link active">Home</a>
                    <a href="/produk" class="nav-item nav-link">Product</a>
                    <a href="{{ Auth::check() ? '/wishlist' : '/login'  }}"  class="nav-item nav-link">Wishlist</a>
                    {{-- <a href="{{ Auth::check() ? '/userbooking' : '/login'  }}"  class="nav-item nav-link">Booking</a> --}}
                    <a href="{{ Auth::check() ? '/userbelimobil' : '/login'  }}"  class="nav-item nav-link">Beli Mobil</a>
                    <a href="{{ Auth::check() ? '/jualmobil' : '/login'  }}"  class="nav-item nav-link">Jual Mobil</a>
                    <a href="{{ Auth::check() ? '/userpengecekan' : '/login'  }}"  class="nav-item nav-link">Pengecekan</a>
                    {{-- <a href="{{ Auth::check() ? '' : '/login'  }}"  class="nav-item nav-link">Akun Saya</a> --}}
                    
                </div>
                <div class="navbar-nav ml-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">{{ Auth::user() ? Auth::user()->nama : 'User Account' }}</a>
                        
                        <div class="dropdown-menu">
                            @if(!Auth::user())
                            <a href="{{ route('login') }}" class="dropdown-item">Login</a>
                            <a href="{{ route('register') }}" class="dropdown-item">Register</a>
                            @else
                            <a class="dropdown-item" onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">Logout</a>
                            
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                            @endif
                        </div>
                        
                    </div>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- Nav Bar End -->  