<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dealer</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="eCommerce HTML Template Free Download" name="keywords">
        <meta content="eCommerce HTML Template Free Download" name="description">

        <!-- Favicon -->
        <link href="{{ asset('/')}}home-asset/img/favicon.ico" rel="icon">
        <link href="{{ asset('/')}}home-asset/lib/slick/slick.css" rel="stylesheet">
        <link href="{{ asset('/')}}home-asset/lib/slick/slick-theme.css" rel="stylesheet">
        
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap" rel="stylesheet">
        
        <!-- CSS Libraries -->
        {{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/b-2.2.3/r-2.3.0/sb-1.3.4/datatables.min.css"/>
        <link href="{{ asset('/')}}home-asset/css/style.css" rel="stylesheet">
        <link href="{{ asset('/')}}home-asset/css/custom.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        @yield("other-css")
    </head>

    <body>
        <div class="btn-wa">
            <a href="https://wa.me/6282118101906" target="_blank"><i class="fab fa-whatsapp fa-2xl icon"></i></a>
        </div>
        @include('layouts.navbar')

         
        <div style="min-height: 90vh">
            @yield("content")
        </div>
        
        
        <!-- Footer Start -->
        @include('layouts.footer')
        
        <!-- Back to Top -->
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        
        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="{{ asset('/')}}home-asset/lib/easing/easing.min.js"></script>
        <script src="{{ asset('/')}}home-asset/lib/slick/slick.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/b-2.2.3/r-2.3.0/sb-1.3.4/datatables.min.js"></script>
        
        <!-- Template Javascript -->
        <script src="{{ asset('/')}}home-asset/js/main.js"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        @stack('script')
        @yield("other-js")
        @yield("init-js")
        @if (session('success') && !is_array(session('success')))
        <script>
                Swal.fire({
                position: 'top-center',
                icon: 'success',
                title: 'Success!',
                text: '{{ session('success') }}',
                showConfirmButton: false,
                timer: 1500
                })        
            </script>
        @endif
        
        @if (session('error') && !is_array(session('error')))
        <script>
                Swal.fire({
                position: 'top-center',
                icon: 'error',
                title: 'Peringatan!',
                text: '{{ session('error') }}',
                showConfirmButton: false,
                timer: 1500
                })        
            </script>
        @endif
    </body>
</html>
