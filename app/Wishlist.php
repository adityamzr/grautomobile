<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 't_wishlist';

    protected $primaryKey = 'id_wishlist';

    protected $fillable = [
        'id_wishlist',
        'id_user',
        'id_mobil'
    ];

    public $timestamps = false;

    public function mobil(){
        return $this->hasOne(Mobil::class, 'id_mobil', 'id_mobil');
    }
}
