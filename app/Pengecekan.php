<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengecekan extends Model
{
    protected $table = 't_pengecekan';

    protected $primaryKey = 'id_pengecekan';

    protected $fillable = [
        'id_pengecekan',
        'id_user',
        'tanggal_pengecekan',
        'jam',
        'tim',
        'status',
        'bukti_trf',
    ];
    
    public $timestamps = false;

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }

    public function mekanik(){
        return $this->hasOne(Mekanik::class, 'id_mekanik', 'tim');
    }

    public function hasilPengecekan(){
        return $this->hasOne(HasilPengecekan::class, 'id_pengecekan', 'id_pengecekan');
    }

    public function orang(){
        return $this->hasOne(Konstanta::class, 'id_kons', 'id_kons');
    }
}
