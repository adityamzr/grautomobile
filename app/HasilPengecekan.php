<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilPengecekan extends Model
{
    protected $table = 't_hasil_pengecekan';

    protected $primaryKey = 'id_hasil_pengecekan';

    protected $fillable = [
        'id_hasil_pengecekan',
        'id_pengecekan',
        'keterangan',
        'deskripsi'
    ];
}
