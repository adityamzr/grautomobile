<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jual extends Model
{
    
    protected $table = 't_jual';

    protected $primaryKey = 'id_jual';

    protected $fillable = [
        'id_jual',
        'id_user',
        'id_mobil',
        'harga_penawaran',
        'alamat',
        'tanggal',
        'jam',
    ];

    public function mobil(){
        return $this->hasOne(Mobil::class, 'id_mobil', 'id_mobil');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }
}
