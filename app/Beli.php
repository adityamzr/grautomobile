<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beli extends Model
{
    protected $table = 't_beli';

    protected $primaryKey = 'id_beli';

    protected $fillable = [
        'id_beli',
        'id_user',
        'id_mobil',
        'status',
        'tanggal',
        'jam',
    ];

    public $timestamps = false;

    public function mobil(){
        return $this->hasOne(Mobil::class, 'id_mobil', 'id_mobil');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }
}
