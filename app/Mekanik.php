<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mekanik extends Model
{
    protected $table = 't_mekanik';

    protected $primaryKey = 'id_mekanik';

    protected $fillable = [
        'id_mekanik',
        'id_pengecekan',
        'nama_mekanik',
    ];
    
    public $timestamps = false;
}
