<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobil extends Model
{
    protected $table = 't_mobil';

    protected $primaryKey = 'id_mobil';

    protected $fillable = [
        'nama_mobil',
        'merk_mobil',
        'bensin',
        'tipe',
        'model',
        'kilometer',
        'transmisi',
        'kepemilikan',
        'kapasitas_mesin',
        'pajak',
        'deskripsi',
        'foto_mobil',
        'foto_mobil2',
        'foto_mobil3',
        'foto_mobil4',
        'harga_mobil',
        'status'
    ];

    public $timestamps = false;
}
