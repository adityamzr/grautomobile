<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konstanta extends Model
{
    protected $table = 't_konstanta';

    protected $primaryKey = 'id_kons';

    protected $fillable = [
        'id_kons',
        'nama',
        'value',
    ];

    public $timestamps = false;

}
