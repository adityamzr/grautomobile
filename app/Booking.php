<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 't_booking';

    protected $primaryKey = 'id_booking';

    protected $fillable = [
        'id_booking',
        'id_mobil',
        'id_user',
        'status',
        'bukti_book',
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'id_user');
    }

    public function mobil(){
        return $this->hasOne(Mobil::class, 'id_mobil', 'id_mobil');
    }
}
