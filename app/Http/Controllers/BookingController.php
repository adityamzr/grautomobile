<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'booking';
        $data['booking'] = Booking::orderBy('id_booking', 'DESC')->get();
        return view('back.pages.booking.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'status' => 'required'
        ]);
        $booking = Booking::find($id);
        if($input['status'] == 'Ditolak'){
            $update = $booking->update([
                'status' => $input['status']
            ]);

            if($update){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menolak booking mobil'
                ]);
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'Gagal menolak booking mobil'
                ]);
            }
        }else{
            $update = $booking->update([
                'status' => $input['status']
            ]);

            if($update){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menerima booking mobil'
                ]);
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'Gagal menerima booking mobil'
                ]);
            }
        }
    }

    public function verifikasi(Request $request)
    {
        dd($request->all());
        $booking = Booking::find($request['id_booking']);
        $update = $booking->update(['status' => 'Booked']);

        if($update){
            return redirect('/booking')->with('success', 'Behasil verifikasi bukti pembayaran');
        }else{
            return redirect()->back()->with('error', 'Gagal melakukan verifikasi bukti pembayaran');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $delete = $booking->delete();

        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus booking mobil',
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Gagal menghapus booking mobil',
            ]);
        }
    }
}
