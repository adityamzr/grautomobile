<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Mobil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data['booking'] = Booking::with('mobil')->where('id_user', $id)->orderBy('id_booking', 'DESC')->get();
        return view('homepage.userbooking.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Auth::user()->id;
        if($request->hasFile('bukti_book')){
            $filename = $request['bukti_book']->hashName();
            $request->file('bukti_book')->storeAs('back/bukti_booking', $filename, 'public');

            $store = Booking::create([
                'bukti_book' => $filename,
                'id_mobil' => $request['id_mobil'],
                'id_user' => $id,
                'status' => 'Booking'
            ]);

            if($store){
                return redirect('/userbooking')->with('success', 'Berhasil upload bukti transfer, mohon tunggu konfirmasi booking anda.');
            }else{
                return redirect()->back()->with('error', 'Gagal upload bukti transfer');
            }
        }else{
            return redirect()->back()->with('error', 'Gagal upload bukti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
