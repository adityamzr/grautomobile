<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function sendEmail()
    {
        $data = [
            'judul' => 'Mencoba email',
            'isi' => 'Selamat anda berhasil mengirim email'
        ];

        Mail::to('adityamzainir@gmail.com')->send(new TestMail($data));
        return "Email Terkirim";
    }
}
