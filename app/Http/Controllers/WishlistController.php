<?php

namespace App\Http\Controllers;

use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data['produk'] = Wishlist::with('mobil')->where('id_user', $id)->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.wishlist', $data);
    }

    public function like(Request $request)
    {
        // dd($id_user);
        $id_mobil = $request['id'];
        $id_user = Auth::user()->id;
        $like = Wishlist::where('id_mobil', $id_mobil)->where('id_user', $id_user)->first();

        if(empty($like)){
            $store = Wishlist::create([
                'id_user' => $id_user,
                'id_mobil' => $id_mobil
            ]);

            if($store){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil memasukan ke wishlist'
                ]);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Gagal memasukan ke wishlist'
                ]);
            }
        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Produk sudah ada di wishlist'
            ]);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $wish = Wishlist::find($id);
        $delete = $wish->delete();
        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus wishlist'
            ]);
        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Gagal menghapus wishlist'
            ]);
        }
    }
}
