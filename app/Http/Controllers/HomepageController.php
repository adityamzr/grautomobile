<?php

namespace App\Http\Controllers;

use App\Mobil;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomepageController extends Controller
{
    public function index() {
        if(Auth::check()){
            $id = Auth::user()->id;
            $data['mobil'] = Mobil::where('status', 'Tersedia')->get();
            $data['count'] = Wishlist::where('id_user', $id)->get()->count();
            return view('homepage.index', $data);
        }
        $data['mobil'] = Mobil::where('status', 'Tersedia')->get();
        return view('homepage.index', $data);
    }
}
