<?php

namespace App\Http\Controllers;

use App\HasilPengecekan;
use App\Konstanta;
use App\Mail\PengecekanBaru;
use App\Mekanik;
use App\Pengecekan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserPengecekanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data['pengecekan'] = Pengecekan::where('id_user', $id)->orderBy('id_pengecekan', 'DESC')->get();
        return view('homepage.userpengecekan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['mekanik'] = Mekanik::where('id_pengecekan', null)->count();
        return view('homepage.userpengecekan.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['kons'] == 0) return redirect()->back()->with('error', 'Tim lapangan sedang tidak tersedia');
        
        $input = $request->validate([
            'tanggal_pengecekan' => 'required',
            'jam' => 'required'
        ]);

        $id_user = Auth::user()->id;
        $kons = (10 - $request['kons']) + 1;

        $store = Pengecekan::create([
            'id_user' => $id_user,
            'tanggal_pengecekan' => $input['tanggal_pengecekan'],
            'jam' => $input['jam'],
            'status' => 'Pengajuan',
            'tim' => $kons
        ]);
        
        $mekanik = Mekanik::where('id_mekanik', $kons)->first();
        $mekanik->update([
            'id_pengecekan' => $store->id_pengecekan
        ]);

        if($store){
            $data['pengecekan'] = Pengecekan::with('user')->where('id_pengecekan', $store->id_pengecekan)->first();
            $users = User::where('role', 4)->get();
            foreach ($users as $user) {
                Mail::to($user->email)->send(new PengecekanBaru($data));
            }
            Mail::to(Auth::user()->email)->send(new PengecekanBaru($data));
            return redirect('/userpengecekan')->with('success', 'Berhasil tambah jadwal pengecekan');
        }else{
            return redirect()->back()->with('error', 'Gagal tambah jadwal pengecekan');
        }
    }

    public function uploadBukti(Request $request)
    {
        $data['pengecekan'] = Pengecekan::with('user')->where('id_pengecekan', $request['id_pengecekan'])->first();

        if($request->hasFile('bukti_trf')){
            $filename = $request['bukti_trf']->hashName();
            $request->file('bukti_trf')->storeAs('back/bukti_transfer', $filename, 'public');

            $upload = $data['pengecekan']->update([
                'bukti_trf' => $filename,
                'status' => 'Verifikasi'
            ]);

            if($upload){
                $users = User::where('role', 4)->get();
                foreach ($users as $user) {
                    Mail::to($user->email)->send(new PengecekanBaru($data));
                }
                Mail::to(Auth::user()->email)->send(new PengecekanBaru($data));
                return redirect('/userpengecekan')->with('success', 'Berhasil upload bukti transfer');
            }else{
                return redirect()->back()->with('error', 'Gagal upload bukti transfer');
            }
        }else{
            return redirect()->back()->with('error', 'Gagal upload bukti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['hasil'] = Pengecekan::with('hasilPengecekan', 'user')->where('id_pengecekan', $id)->first();
        return view('homepage.userpengecekan.hasil', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['mekanik'] = Mekanik::where('id_pengecekan', null)->count();
        $data['pengecekan'] = Pengecekan::find($id);
        return view('homepage.userpengecekan.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'tanggal_pengecekan' => 'required',
            'jam' => 'required'
        ]);

        $pengecekan = Pengecekan::find($id);
        $update = $pengecekan->update([
            'tanggal_pengecekan' => $input['tanggal_pengecekan'],
            'jam' => $input['jam']
        ]);

        if($update){
            return redirect('/userpengecekan')->with('success', 'Berhasil ubah jadwal pengecekan');
        }else{
            return redirect()->back()->with('error', 'Gagal ubah jadwal pengecekan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengecekan = Pengecekan::find($id);
        $delete = $pengecekan->delete();


        $mekanik = Mekanik::where('id_pengecekan', $id)->first();
        $mekanik->update([
            'id_pengecekan' => null
        ]);

        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus jadwal pengecekan',
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Gagal menghapus jadwal pengecekan',
            ]);
        }
    }
}
