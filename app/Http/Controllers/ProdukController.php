<?php

namespace App\Http\Controllers;

use App\Beli;
use App\Booking;
use App\Mobil;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function suv()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'Suv')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function sedan()
    {
        $data['produk'] = Mobil::where('status', 'tersedia', 'DESC')->where('model', 'Sedan')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function double()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'Double')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function pickup()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'Pickup')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function diesel()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'Diesel')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function city()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'City')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    public function sport()
    {
        $data['produk'] = Mobil::where('status', 'tersedia')->where('model', 'Sport')->orderBy('id_mobil', 'DESC')->get();
        return view('homepage.produk', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['mobil'] = Mobil::find($id);
        $data['beli'] = Beli::where('id_mobil', $id)->first();
        return view('homepage.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
