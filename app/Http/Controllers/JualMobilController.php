<?php

namespace App\Http\Controllers;

use App\Jual;
use App\Mail\NotifJual;
use App\Mail\NotifJualLanjut;
use App\Mobil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class JualMobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $data['jual'] = Jual::with('mobil')->where('id_user', $id, 'DESC')->get();
        return view('homepage.jualmobil.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('homepage.jualmobil.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'nama_mobil' => 'required',
            'merk_mobil' => 'required',
            'bensin' => 'required',
            'kilometer' => 'required',
            'transmisi' => 'required',
            'kepemilikan' => 'required',
            'kapasitas_mesin' => 'required',
            'pajak' => 'required',
            'deskripsi' => 'required',
            'tahun_produksi' => 'required',
            'model' => 'required',
            'tipe' => 'required',
            'harga_mobil' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $mobil = new Mobil();
            $mobil->nama_mobil = $input['nama_mobil'];
            $mobil->merk_mobil = $input['merk_mobil'];
            $mobil->bensin = $input['bensin'];
            $mobil->kilometer = $input['kilometer'];
            $mobil->transmisi = $input['transmisi'];
            $mobil->kepemilikan = $input['kepemilikan'];
            $mobil->kapasitas_mesin = $input['kapasitas_mesin'];
            $mobil->pajak = $input['pajak'];
            $mobil->deskripsi = $input['deskripsi'];
            $mobil->tahun_produksi = $input['tahun_produksi'];
            $mobil->model = $input['model'];
            $mobil->tipe = $input['tipe'];
            $mobil->harga_mobil = $input['harga_mobil'];
            $mobil->status = 'Pengajuan';
            foreach($request->files->all() as $key=>$item){
                $filename = $request["$key"]->hashName();
                $request->file($key)->storeAs("back/foto_mobil", $filename, 'public');
                $mobil->$key = $filename;
            }
            
            $mobil->save();
            
            $jual = new Jual();
            $jual->id_user = Auth::user()->id;
            $jual->id_mobil = $mobil->id_mobil;
            $jual->harga_penawaran = $input['harga_mobil'];
            $jual->save();

            DB::commit();

            $data['jual'] = Jual::with('user')->where('id_jual', $jual->id_jual)->first();
            $users = User::where('role', 3)->get();
            foreach ($users as $user) {
                Mail::to($user->email)->send(new NotifJual($data));
            }
            Mail::to(Auth::user()->email)->send(new NotifJual($data));

            return redirect('/jualmobil')->with('success', 'Berhasil tambah data');

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['jual'] = Jual::with('mobil')->where('id_jual', $id)->first();
        return view('homepage.jualmobil.detail', $data);
    }

    public function nego(Request $request){

        $input = $request->validate([
            'alamat' => 'required',
            'tanggal' => 'required',
            'jam' => 'required'
        ]);
        DB::beginTransaction();
        try {
            $jual = Jual::find($request['id_jual']);
            $jual->alamat = $input['alamat'];
            $jual->tanggal = $input['tanggal'];
            $jual->jam = $input['jam'];

            $jual->save();

            DB::commit();

            $data['jual'] = Jual::with('user')->where('id_jual', $jual->id_jual)->first();
            $users = User::where('role', 3)->get();
            foreach ($users as $user) {
                Mail::to($user->email)->send(new NotifJualLanjut($data));
            }
            Mail::to(Auth::user()->email)->send(new NotifJualLanjut($data));
            return redirect('/jualmobil')->with('success', 'Berhasil menambah alamat');

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['jual'] = Jual::with('mobil')->where('id_jual', $id)->first();
        return view('homepage.jualmobil.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'nama_mobil' => 'required',
            'merk_mobil' => 'required',
            'bensin' => 'required',
            'kilometer' => 'required',
            'transmisi' => 'required',
            'kepemilikan' => 'required',
            'kapasitas_mesin' => 'required',
            'pajak' => 'required',
            'deskripsi' => 'required',
            'tahun_produksi' => 'required',
            'model' => 'required',
            'tipe' => 'required',
            'harga_mobil' => 'required',
        ]);
        $jual = Jual::find($id);
        $id_mobil = $jual->id_mobil;

        DB::beginTransaction();
        try {
            $mobil = Mobil::find($id_mobil);
            $mobil->nama_mobil = $input['nama_mobil'];
            $mobil->merk_mobil = $input['merk_mobil'];
            $mobil->bensin = $input['bensin'];
            $mobil->kilometer = $input['kilometer'];
            $mobil->transmisi = $input['transmisi'];
            $mobil->kepemilikan = $input['kepemilikan'];
            $mobil->kapasitas_mesin = $input['kapasitas_mesin'];
            $mobil->pajak = $input['pajak'];
            $mobil->deskripsi = $input['deskripsi'];
            $mobil->tahun_produksi = $input['tahun_produksi'];
            $mobil->model = $input['model'];
            $mobil->tipe = $input['tipe'];
            $mobil->harga_mobil = $input['harga_mobil'];
            $mobil->status = $request['status'];
            foreach($request->files->all() as $key=>$item){
                $oldfilename = $mobil->$key;
                $path = public_path("storage/back/foto_mobil/$oldfilename");
                if(File::exists($path)){
                    File::delete($path);
                }
                $newfilename = $request["$key"]->hashName();
                $mobil->$key = $newfilename;
                $request->file($key)->storeAs("back/foto_mobil", $newfilename, 'public');
            }

            $mobil->save();

            DB::commit();

            return redirect('/jualmobil')->with('success', 'Berhasil ubah data');

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jual = Jual::find($id);
        $mobil = Mobil::find($jual['id_mobil']);
        $filename = $mobil['foto_mobil'];
        $filename2 = $mobil['foto_mobil2'];
        $filename3 = $mobil['foto_mobil3'];
        $filename4 = $mobil['foto_mobil4'];
        $path = [
            public_path("storage/back/foto_mobil/$filename"),
            public_path("storage/back/foto_mobil/$filename2"),
            public_path("storage/back/foto_mobil/$filename3"),
            public_path("storage/back/foto_mobil/$filename4")
        ];

        $delete = $mobil->delete();
        if($delete){
            for($i = 0; $i < count($path); $i++){
                if(File::exists($path[$i])){
                    File::delete($path[$i]);
                }
            }
            
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data!'
            ]);
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data!');
        }
    }
}
