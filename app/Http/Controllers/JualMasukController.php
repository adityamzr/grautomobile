<?php

namespace App\Http\Controllers;

use App\Jual;
use App\Mail\JualMobil;
use App\Mail\NotifJual;
use App\Mail\NotifJualLanjut;
use App\Mobil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade\Pdf;

class JualMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'jualmasuk';
        $data['jual'] = Jual::with('user')->whereHas('mobil', function($query){
          $query->where('status', '!=', 'Tersedia');
        })->orderBy('id_jual', 'DESC')->get();
        return view('back.pages.jualmasuk.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'jualmasuk';
        $data['jual'] = Jual::with(['mobil', 'user'])->where('id_mobil', $id)->first();
        return view('back.pages.jualmasuk.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'status' => 'required',
        ]);

        $data['jual'] = Jual::with('user')->where('id_mobil', $id)->first();
        
        DB::beginTransaction();
        try {
            $mobil = Mobil::find($id);
            $mobil->status = $input['status'];
            $mobil->save();

            $data['mobil'] = $mobil;
            $getDate = Carbon::now();
            $getDate = $getDate->toArray();
            $currentDate = $getDate['day'].'-'.$getDate['month'].'-'.$getDate['year'];
            $data['tanggal'] = $currentDate;
            if($data['mobil']->status != 'Nego'){
                $toUser = $data['jual']->user->email;
                $toAdmin = Auth::user()->email;
                Mail::to("$toUser")->send(new JualMobil($data));
                Mail::to("$toAdmin")->send(new JualMobil($data));
            }else{
                $toUser = $data['jual']->user->email;
                $toAdmin = Auth::user()->email;
                Mail::to("$toUser")->send(new NotifJualLanjut($data));
                Mail::to("$toAdmin")->send(new NotifJualLanjut($data));
            }
            
            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil mengubah status'
            ]);

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jual = Jual::find($id);
        $mobil = Mobil::find($jual['id_mobil']);
        $filename = $mobil['foto_mobil'];
        $filename2 = $mobil['foto_mobil2'];
        $filename3 = $mobil['foto_mobil3'];
        $filename4 = $mobil['foto_mobil4'];
        $path = [
            public_path("storage/back/foto_mobil/$filename"),
            public_path("storage/back/foto_mobil/$filename2"),
            public_path("storage/back/foto_mobil/$filename3"),
            public_path("storage/back/foto_mobil/$filename4")
        ];

        $delete = $jual->delete();
        $mobil->delete();
        if($delete){
            for($i = 0; $i < count($path); $i++){
                if(File::exists($path[$i])){
                    File::delete($path[$i]);
                }
            }
            
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data!'
            ]);
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data!');
        }
    }

    public function cetakPDF()
    {
        $data['jual'] = Jual::with('mobil', 'user')->orderBy('id_jual', 'DESC')->get();
        if(empty($data['jual'])) return redirect()->back()->with('error', 'Tidak bisa mencetak laporan karena data masih kosong');
        $getDate = Carbon::now();
        $getDate = $getDate->toArray();
        $currentDate = $getDate['day'].'-'.$getDate['month'].'-'.$getDate['year'];
        $data['tanggal'] = $currentDate;        

        // return view('back.pages.jualmasuk.laporan', $data);
        $pdf = PDF::loadview('back.pages.jualmasuk.laporan', $data)->setPaper('a4', 'landscape');
    	return $pdf->download('laporan_jualmasuk_tanggal_'.$data['tanggal'].'.pdf');
    }
}
