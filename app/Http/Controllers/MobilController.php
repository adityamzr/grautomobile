<?php

namespace App\Http\Controllers;

use App\Beli;
use DataTables;
use App\Mobil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Barryvdh\DomPDF\Facade\Pdf;

class MobilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'mobil';
        $data['mobil'] = Mobil::where('status', 'Tersedia')->orwhere('status', 'Terjual')->orderBy('id_mobil', 'DESC')->get();
        return view('back.pages.mobil.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'mobil';
        return view('back.pages.mobil.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'nama_mobil' => 'required',
            'merk_mobil' => 'required',
            'bensin' => 'required',
            'kilometer' => 'required',
            'transmisi' => 'required',
            'kepemilikan' => 'required',
            'kapasitas_mesin' => 'required',
            'pajak' => 'required',
            'deskripsi' => 'required',
            'tahun_produksi' => 'required',
            'model' => 'required',
            'tipe' => 'required',
            'harga_mobil' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $mobil = new Mobil();
            $mobil->nama_mobil = $input['nama_mobil'];
            $mobil->merk_mobil = $input['merk_mobil'];
            $mobil->bensin = $input['bensin'];
            $mobil->kilometer = $input['kilometer'];
            $mobil->transmisi = $input['transmisi'];
            $mobil->kepemilikan = $input['kepemilikan'];
            $mobil->kapasitas_mesin = $input['kapasitas_mesin'];
            $mobil->pajak = $input['pajak'];
            $mobil->deskripsi = $input['deskripsi'];
            $mobil->tahun_produksi = $input['tahun_produksi'];
            $mobil->model = $input['model'];
            $mobil->tipe = $input['tipe'];
            $mobil->harga_mobil = $input['harga_mobil'];
            $mobil->status = 'Tersedia';
            foreach($request->files->all() as $key=>$item){
                $filename = $request["$key"]->hashName();
                $request->file($key)->storeAs("back/foto_mobil", $filename, 'public');
                $mobil->$key = $filename;
            }
            $mobil->save();

            DB::commit();

            return redirect('/mobil')->with('success', 'Berhasil tambah data');

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['title'] = 'mobil';
        $data['mobil'] = Mobil::find($id);
        $data['beli'] = Beli::with('user')->where('id_mobil', $id)->first();
        return view('back.pages.mobil.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'mobil';
        $data['mobil'] = Mobil::find($id);
        return view('back.pages.mobil.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'nama_mobil' => 'required',
            'merk_mobil' => 'required',
            'bensin' => 'required',
            'kilometer' => 'required',
            'transmisi' => 'required',
            'kepemilikan' => 'required',
            'kapasitas_mesin' => 'required',
            'pajak' => 'required',
            'deskripsi' => 'required',
            'tahun_produksi' => 'required',
            'model' => 'required',
            'tipe' => 'required',
            'harga_mobil' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $mobil = Mobil::find($id);
            $mobil->nama_mobil = $input['nama_mobil'];
            $mobil->merk_mobil = $input['merk_mobil'];
            $mobil->bensin = $input['bensin'];
            $mobil->kilometer = $input['kilometer'];
            $mobil->transmisi = $input['transmisi'];
            $mobil->kepemilikan = $input['kepemilikan'];
            $mobil->kapasitas_mesin = $input['kapasitas_mesin'];
            $mobil->pajak = $input['pajak'];
            $mobil->deskripsi = $input['deskripsi'];
            $mobil->tahun_produksi = $input['tahun_produksi'];
            $mobil->model = $input['model'];
            $mobil->tipe = $input['tipe'];
            $mobil->harga_mobil = $input['harga_mobil'];
            $mobil->status = 'Tersedia';
            foreach($request->files->all() as $key=>$item){
                $oldfilename = $mobil->$key;
                $path = public_path("storage/back/foto_mobil/$oldfilename");
                if(File::exists($path)){
                    File::delete($path);
                }
                $newfilename = $request["$key"]->hashName();
                $mobil->$key = $newfilename;
                $request->file($key)->storeAs("back/foto_mobil", $newfilename, 'public');
            }
            $mobil->save();

            DB::commit();

            return redirect('/mobil')->with('success', 'Berhasil ubah data');

        } catch (\Exception $e){
            DB::rollBack();
            return ["error" => $e->getMessage()];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = Mobil::find($id);
        $path = [
            public_path("storage/back/foto_mobil/$mobil->foto_mobil"),
            public_path("storage/back/foto_mobil/$mobil->foto_mobil2"),
            public_path("storage/back/foto_mobil/$mobil->foto_mobil3"),
            public_path("storage/back/foto_mobil/$mobil->foto_mobil4"),
        ];

        $delete = $mobil->delete();
        if($delete){
            for($i = 0; $i < count($path); $i++){
                if(File::exists($path[$i])){
                    File::delete($path[$i]);
                }
            }
            
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data!'
            ]);
        }else{
            return redirect()->back()->with('error', 'Gagal menghapus data!');
        }

    }

    public function cetakPDF()
    {
        $data['mobil'] = Mobil::where('status', 'Tersedia')->orwhere('status', 'Terjual')->orderBy('id_mobil', 'DESC')->get();
        if(empty($data['mobil'])) return redirect()->back()->with('error', 'Tidak bisa mencetak laporan karena data masih kosong');
        $getDate = Carbon::now();
        $getDate = $getDate->toArray();
        $currentDate = $getDate['day'].'-'.$getDate['month'].'-'.$getDate['year'];
        $data['tanggal'] = $currentDate;        

        // return view('back.pages.mobil.laporan', $data);
        $pdf = PDF::loadview('back.pages.mobil.laporan', $data)->setPaper('a4', 'landscape');
    	return $pdf->download('laporan_mobil_tanggal_'.$data['tanggal'].'.pdf');
    }
}
