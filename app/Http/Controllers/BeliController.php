<?php

namespace App\Http\Controllers;

use App\Beli;
use App\Mail\BeliMobil;
use App\Mobil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class BeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['status'] = 'Pengajuan';

        $store = Beli::create($input);
        
        $data['beli'] = Beli::with('user')->where('id_mobil', $input['id_mobil'])->first();
        $id_mobil = $input['id_mobil'];
        $users = User::where('role', 2)->get();
        foreach ($users as $user) {
            Mail::to($user->email)->send(new BeliMobil($data));
        }
        Mail::to($data['beli']->user->email)->send(new BeliMobil($data));

        if($store){
            return redirect('/userbelimobil')->with('success', 'Berhasil mengirim jadwal kunjungan, selalu cek email anda untuk pemberitahuan selanjutnya.');
        }else{
            return redirect()->back()->with('error', 'Gagal mengirim jadwal kunjungan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $data['beli'] = Beli::with('user', 'mobil')->where('id_beli', $id)->first();
        if($input['status'] == 'Nego'){
            $update = $data['beli']->update([
                'status' => $input['status']
            ]);

            Mail::to(Auth::user()->email)->send(new BeliMobil($data));
            Mail::to($data['beli']->user->email)->send(new BeliMobil($data));
        }elseif($input['status'] == 'Deal'){
            $update = $data['beli']->update([
                'status' => $input['status']
            ]);

            $mobil = Mobil::where('id_mobil', $data['beli']->id_mobil)->first();
            $mobil->update(['status' => 'Terjual']);

            Mail::to(Auth::user()->email)->send(new BeliMobil($data));
            Mail::to($data['beli']->user->email)->send(new BeliMobil($data));
        }else{
            Mail::to(Auth::user()->email)->send(new BeliMobil($data));
            Mail::to($data['beli']->user->email)->send(new BeliMobil($data));
            $update = Beli::find($id)->delete();
        }

        if($update){
            return response()->json();
        }else{
            return response()->json();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
