<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'user';
        $data['user'] = User::all();
        return view('back.pages.user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = 'user';
        return view('back.pages.user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'nama' => 'required',
            'nohp' => 'required',
            'email' => 'required',
            'password' => 'required',
            'alamat' => 'required',
            'role' => 'required'
        ]);

        $store = User::create([
            'nama' => $input['nama'],
            'nohp' => $input['nohp'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'alamat' => $input['alamat'],
            'role' => $input['role']
        ]);

        if($store){
            return redirect('/user')->with('success', 'Berhasil tambah data user');
        }else{
            return redirect()->back()->with('success', 'Berhasil tambah data user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'user';
        $data['user'] = User::find($id);
        return view('back.pages.user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'nama' => 'required',
            'nohp' => 'required',
            'email' => 'required',
            'password' => '',
            'alamat' => 'required',
            'role' => 'required'
        ]);

        
        $user = User::find($id);
        if($input['password'] != null){
            $password = bcrypt($input['password']);
        }else{
            $password = $user['password'];
        }
        $update = $user->update([
            'nama' => $input['nama'],
            'nohp' => $input['nohp'],
            'email' => $input['email'],
            'password' => $password,
            'alamat' => $input['alamat'],
            'role' => $input['role']
        ]);

        if($update){
            return redirect('/user')->with('success', 'Berhasil mengubah data user');
        }else{
            return redirect()->back()->with('success', 'Berhasil mengubah data user');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $delete = $user->delete();
        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus data',
            ]);
        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Gagal menghapus data',
            ]);
        }
    }
}
