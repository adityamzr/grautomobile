<?php

namespace App\Http\Controllers;

use App\HasilPengecekan;
use App\Konstanta;
use App\Mail\KeputusanCek;
use App\Mail\PengecekanBaru;
use App\Mail\TestMail;
use App\Mekanik;
use App\Pengecekan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade\Pdf;

class PengecekanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = 'pengecekan';
        $data['pengecekan'] = Pengecekan::orderBy('id_pengecekan', 'DESC')->get();
        return view('back.pages.pengecekan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'keterangan' => 'required',
            'deskripsi' => 'required',
        ]);
        $data['pengecekan'] = Pengecekan::find($request->id_pengecekan);
        $toEmail = $data['pengecekan']->user->email;
        $getDate = Carbon::now();
        $getDate = $getDate->toArray();
        $currentDate = $getDate['day'].'-'.$getDate['month'].'-'.$getDate['year'];
        $data['tanggal'] = $currentDate;

        $data['hasilPengecekan'] = HasilPengecekan::create([
            'id_pengecekan' => $request->id_pengecekan,
            'keterangan' => $input['keterangan'],
            'deskripsi' => $input['deskripsi'],
        ]);

        $update = $data['pengecekan']->update(['status' => 'Selesai']);

        $mekanik = Mekanik::where('id_pengecekan', $data['pengecekan']->id_pengecekan)->first();
        $mekanik->update([
            'id_pengecekan' => null
        ]);
        
        if($update){
            $toAdmin = Auth::user()->email;
            foreach (["$toAdmin", "$toEmail"] as $recipient) {
                Mail::to($recipient)->send(new TestMail($data));
            }
            return redirect('/pengecekan')->with('success', 'Berhasil melakukan pengecekan');
        }else{
            return redirect()->back()->with('error', 'Gagal melakukan pengecekan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'pengecekan';
        // $data['pengecekan'] = Pengecekan::with('user')->whereHas('hasilPengecekan', function($query, $id){
        //     $query->where('id_pengecekan', $id);
        // });
        $data['pengecekan'] = Pengecekan::find($id);
        return view('back.pages.pengecekan.form', $data);
    }

    public function editUpdate(Request $request, $id)
    {
        
        

    }

    public function verifikasi(Request $request)
    {
        $data['pengecekan'] = Pengecekan::with('user')->where('id_pengecekan', $request['id_pengecekan'])->first();
        $update = $data['pengecekan']->update(['status' => 'Terima']);

        if($update){
            Mail::to(Auth::user()->email)->send(new PengecekanBaru($data));
            Mail::to($data['pengecekan']->user->email)->send(new PengecekanBaru($data));
            return redirect('/pengecekan')->with('success', 'Behasil verifikasi bukti pembayaran');
        }else{
            return redirect()->back()->with('error', 'Gagal melakukan verifikasi bukti pembayaran');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->validate([
            'status' => 'required'
        ]);
        $data['pengecekan'] = Pengecekan::with('user')->where('id_pengecekan', $id)->first();
        if($input['status'] == 'Tolak'){
            $update = $data['pengecekan']->update([
                'status' => $input['status']
            ]);

        $mekanik = Mekanik::where('id_pengecekan', $data['pengecekan']->id_pengecekan)->first();
        $mekanik->update([
            'id_pengecekan' => null
        ]);

            if($update){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menolak jadwal pengecekan'
                ]);
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'Gagal menolak jadwal pengecekan'
                ]);
            }

        }else{
            $update = $data['pengecekan']->update([
                'status' => $input['status']
            ]);

            if($update){
                Mail::to(Auth::user()->email)->send(new PengecekanBaru($data));
                Mail::to($data['pengecekan']->user->email)->send(new PengecekanBaru($data));
                return response()->json([
                    'status' => 'success',
                    'message' => 'Berhasil menerima jadwal pengecekan'
                ]);
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' => 'Gagal menerima jadwal pengecekan'
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengecekan = Pengecekan::find($id);
        $delete = $pengecekan->delete();

        if($delete){
            return response()->json([
                'status' => 'success',
                'message' => 'Berhasil menghapus pengecekan',
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Gagal menghapus pengecekan',
            ]);
        }
    }

    public function cetakPDF()
    {
        $data['pengecekan'] = Pengecekan::with('hasilPengecekan', 'user')->where('status', 'Selesai')->orderBy('id_pengecekan', 'DESC')->get();
        if(empty($data['pengecekan'])) return redirect()->back()->with('error', 'Tidak bisa mencetak laporan karena data masih kosong');
        $getDate = Carbon::now();
        $getDate = $getDate->toArray();
        $currentDate = $getDate['day'].'-'.$getDate['month'].'-'.$getDate['year'];
        $data['tanggal'] = $currentDate;        

        // return view('back.pages.pengecekan.laporan', $data);
        $pdf = PDF::loadview('back.pages.pengecekan.laporan', $data)->setPaper('a4', 'portrait');
    	return $pdf->download('laporan_pengecekan_tanggal_'.$data['tanggal'].'.pdf');
    }
}
