<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// Route::post('/login',[LoginController::class, 'index']);

Route::get('/', 'HomepageController@index');

Route::resource('produk', 'ProdukController');

Route::get('send-email', 'MailController@sendEmail');

Route::get('produk/search/suv', 'ProdukController@suv');
Route::get('produk/search/sedan', 'ProdukController@sedan');
Route::get('produk/search/double-cabin', 'ProdukController@double');
Route::get('produk/search/pickup', 'ProdukController@pickup');
Route::get('produk/search/diesel', 'ProdukController@diesel');
Route::get('produk/search/city-car', 'ProdukController@city');
Route::get('produk/search/sport', 'ProdukController@sport');
// Route::get('/home', 'HomeController@index')->name('home');
// Route::resource('dashboard', DashboardController::class);

Route::middleware('auth')->group(function(){
    Route::resource('/wishlist', 'WishlistController');
    Route::post('/produk/like', 'WishlistController@like');

    Route::get('/home-page', 'HomepageController@index')->name('homepage');

    // USER
    Route::post('jualmobil/nego', 'JualMobilController@nego');
    Route::resource('jualmobil', 'JualMobilController');
    

    Route::resource('userpengecekan', 'UserPengecekanController');
    Route::post('userpengecekan/upload', 'UserPengecekanController@uploadBukti');
    
    Route::resource('/userbooking', 'UserBookingController');
    
    // ADMIN
    Route::resource('dashboard', 'DashboardController');
    
    Route::get('mobil/cetakPDF', 'MobilController@cetakPDF');
    Route::resource('mobil', 'MobilController');

    Route::resource('userbelimobil', 'UserBeliMobilController');
    
    Route::patch('mobil/belimobil/{id}', 'BeliController@update');
    Route::resource('belimobil', 'BeliController')->except('update');
    // Route::post('booking/verifikasi', 'BookingController@verifikasi');

    Route::get('jualmasuk/cetakPDF', 'JualMasukController@cetakPDF');
    Route::resource('jualmasuk', 'JualMasukController');

    Route::get('pengecekan/cetakPDF', 'PengecekanController@cetakPDF');
    Route::post('pengecekan/verifikasi', 'PengecekanController@verifikasi');
    Route::patch('pengecekan/update/{id}', 'PengecekanController@editUpdate');
    Route::resource('pengecekan', 'PengecekanController');
    
    Route::resource('user', 'UserController');
});